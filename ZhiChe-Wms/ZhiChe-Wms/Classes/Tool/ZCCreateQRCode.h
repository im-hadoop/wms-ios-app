//
//  ZCCreateQRCode.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/23.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCCreateQRCode : NSObject


+ (UIImage *)createQRCodeFromString:(NSString *)string
                          codeSize:(CGFloat)codesize;

@end
