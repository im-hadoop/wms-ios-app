//
//  httpAddress.h
//  kangbs
//
//  Created by 高睿婕 on 2018/8/13.
//  Copyright © 2018年 Regina. All rights reserved.
//

#ifndef httpAddress_h
#define httpAddress_h

//      ----------------- 用户信息
//获取用户Token
#define  UserToken           @"oauth/token"

//获取用户info
#define  UserInfo            @"user/info"

//修改密码
#define  UpdatePwd           @"account/updatePassword"

//我的任务
#define  MyTask              @"task/myTask"


//      -----------------  提车任务
//任务列表
#define  TaskList            @"task/getTaskList"

//任务详情
#define  TaskDetail          @"task/getTaskDetailByTask"

//完成移车
#define  FinishMove          @"task/finishForMove"

//开始提车
#define  StartPick           @"task/updateStartTaskForPick"

//首页搜索
#define  HomeSearch          @"task/getOrderInfo"

//任务赋码
#define  BindQRCode          @"model/qrCodeBind/bindCodeAll"

//扫码查询
#define  SearchByQRCode      @"task/getTaskDetailByQrCode"

//车架号录入
#define  EditVin             @"orderRelease/updateVin"



//                --------  异常相关
//异常总数
#define  ExceptionTotal      @"commonException/getExceptionTotal"

//九宫格异常信息查询
#define  ExceptionInfo       @"commonException/getExceptionInfo"

//标记异常配置列表
#define  ConfigurationList   @"exceptionConfiguration/configurationList"

//标记异常
#define  SignException       @"exceptionRegister/noteException"

//获取已标记的异常列表
#define  ExceptionPicUrl     @"exceptionRegister/exceptionPicUrl"

//全部异常
#define  AllException        @"commonException/getAllExceptionPicUrl"

//获取缺件列表
#define  MissingList         @"missingRegister/getMissingInfo"

//编辑缺件
#define  EditMissingInfo     @"missingRegister/updateMissingInfo"

//                 --------  上传图片
//获取七牛的token
#define  QiniuToken          @"qiNiu/uploadToken"

//获取七牛上图片的Url
#define  QiniuImageUrl       @"qiNiu/downloadUrl"


//              ---------    收车质检
//获取未质检列表
#define  QualityTestingList       @"inboundInspectLine/selectList"

//获取未质检详情
#define  QualityTestingDetail     @"inboundInspectLine/selectDetail"

//完成质检
#define  QualityTestingCommit     @"inboundInspectLine/inboundInspect"

//              ------------  入库
//入库计划列表
#define  SelectPlanByPage         @"inboundNoticeLine/selectPlanByPage"

//入库详情
#define  PutInStoragePlanDetail   @"inboundNoticeLine/selectById"

//分配入库列表
#define  PutInStorageAllocList    @"inboundNoticeLine/selectByPage"

//入库确认
#define  PutInStorageCommit       @"inboundNoticeLine/inbound"


//          ----------------   出库
//出库计划列表
#define  OutboundPlanList         @"outboundNoticeLine/getOutboundNoticeList"

//出库详情
#define  OutboundDetail           @"outboundNoticeLine/selectById"

//出库确认列表
#define  OutboundList             @"outboundNoticeLine/selectByPage"

//出库确认
#define  OutboundCommit           @"outboundNoticeLine/outbound"

//        -------------------  备料任务
//备料任务列表

//以板车为单元
#define  PrepareList              @"outboundPrepare/getPrepareVehicle"
//以车辆为单元
//#define  PrepareList            @"outboundPrepare/queryList"

//备料明细
#define  PrepareListDetail        @"outboundPrepare/getVehicleDetail"

//备料任务详情
#define  PrepareDetail            @"outboundPrepare/getPrepare"

//备料提交
#define  PrepareCommit            @"outboundPrepare/prepareConfirm"

//一键领取
#define  GetPrepareTask           @"outboundPrepare/getTheTask"


//         -------------------  库位调整
//库存列表
#define  AdjustmentList           @"stock/queryPage"

//库存详情
#define  AdjustmentDetail         @"stock/getStockInfo"

//库位列表
#define  AdjustmentLocation       @"storeLocation/queryLocationPage"

//调整库位
#define  AdjustmentCommit         @"stock/updateStockLocation"


//       ----------------------  装车交验
//交验列表
#define  LoadingList              @"orderRelease/queryReleaseShip"

//交验详情
#define  LoadingDetail            @"orderRelease/getReleaseShipDetail"

//交验确认
#define  LoadingCommit            @"orderRelease/confirmReleaseShip"

//         -------------------   确认发运
// 运单列表
#define  ShipmentList             @"orderRelease/queryShipList"

//运单详情
#define  ShipmentDetail           @"orderRelease/getShipDetail"

//确认发运
#define  ShipmentCommit           @"orderRelease/updateShip"


//        --------------------     版本更新
#define  AppVersion               @"version/ios/latest"


//         ----------------   新增
//实车确认
#define  BindStoreDetail          @"inboundNoticeLine/bindStoreDetail"

//实车校验
#define  VehicleCheck             @"outboundPrepare/realVehicleCheck"

//异常发运
#define  ExceptionFinish          @"exceptionRegister/exceptionFinish"

//车型列表
#define  VehicleList              @"orderRelease/queryVehicleInfo"

//修改车型信息
#define  UpdateVehicle            @"orderRelease/updateVehicleInfo"

//退库列表
#define  CancellingStockList          @"stock/queryCancelStoreLog"

//退库任务详情
#define  CancellingStockDetail        @"stock/queryCancelStoreDetail"

//领取退库任务
#define  PickUpCancelTask            @"stock/driverReceiveTask"


#endif /* httpAddress_h */
