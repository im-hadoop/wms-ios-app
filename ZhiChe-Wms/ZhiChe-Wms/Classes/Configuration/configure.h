//
//  configure.h
//  kangbs
//
//  Created by 高睿婕 on 2018/8/13.
//  Copyright © 2018年 Regina. All rights reserved.
//

#ifndef configure_h
#define configure_h

//屏幕的宽和高
#define SCREENWIDTH   [UIScreen mainScreen].bounds.size.width
#define SCREENHEIGHT  [UIScreen mainScreen].bounds.size.height

//缩放比
#define AppScale    [UIScreen mainScreen].bounds.size.width/320.0
#define AppHeight   [UIScreen mainScreen].bounds.size.height/568.0

//判断是否为iphone
#define IS_IPHONE  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define Iphone5s         (IS_IPHONE && SCREENHEIGHT == 568.0)
#define Iphone6_7_8      (IS_IPHONE && SCREENHEIGHT == 667.0)
#define Iphone6p_7p_8P   (IS_IPHONE && SCREENHEIGHT == 736.0)
#define IphoneX          (IS_IPHONE && SCREENHEIGHT == 812.0)

//16进制颜色和透明度
#define ZCColor(value,a) [UIColor colorWithRed:((value & 0xFF0000) >> 16)/255.0 green:((value & 0xFF00) >> 8)/255.0 blue:(value & 0xFF)/255.0 alpha:a]

//字体大小
#define FontSize(size)  size/2*AppScale

//布局间距
#define space(s) s/2

//图片大小
#define ImageSize(s) s/2

//登录token的key
#define loginToken  @"loginToken"

//刷新token
#define refreshToken  @"refreshToken"

//有效时长
#define effectiveTime   @"expiresIn"

//token创建时间
#define tokenCreateTime  @"gmtCreate"

//账户Id
#define AccountId      @"accountId"

//用户code
#define UserCode      @"userCode"

//用户name
#define UserName      @"userName"

//用户mobile
#define UserMobile    @"userMobile"

//用户email
#define UserEmail     @"userEmail"

//用户可操作的仓库Id
#define UserStoreId   @"userStoreId"

//用户可操作的仓库名
#define UserStoreName   @"userStoreName"

//用户发车点
#define UserOriginId  @"userOriginId"

//消息提示时间
#define HudTime      2

#define SuccessTime    1


#endif /* configure_h */
