//
//  ZCMineTableHeaderView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/28.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCMineTableHeaderView.h"

@interface ZCMineTableHeaderView ()

@property (nonatomic, strong) UIImageView *titleImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIImageView *indicatorImageView;
@property (nonatomic, strong) UIView *line;
@end

@implementation ZCMineTableHeaderView
#pragma - 懒加载
- (UIImageView *)titleImageView {
    if (!_titleImageView) {
        _titleImageView = [[UIImageView alloc] init];
        _titleImageView.image = [UIImage imageNamed:@"register_password"];
    }
    return _titleImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = ZCColor(0x000000, 0.54);
        _titleLabel.font = [UIFont systemFontOfSize:FontSize(30)];
    }
    return _titleLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [[UILabel alloc] init];
        _descLabel.textColor = ZCColor(0x000000, 0.87);
        _descLabel.font = [UIFont systemFontOfSize:FontSize(30)];
    }
    return _descLabel;
}

- (UIImageView *)indicatorImageView {
    if (!_indicatorImageView) {
        _indicatorImageView = [[UIImageView alloc] init];
        _indicatorImageView.image = [UIImage imageNamed:@"箭头"];
    }
    return _indicatorImageView;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _line;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.titleImageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.descLabel];
        [self addSubview:self.indicatorImageView];
        [self addSubview:self.line];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

#pragma mark - 手势
- (void)tap:(UITapGestureRecognizer *)tap {
    if ([self.delegate respondsToSelector:@selector(mineTableHeaderViewTapWithIndex:)]) {
        [self.delegate mineTableHeaderViewTapWithIndex:self.index];
    }
}

#pragma mark - 属性方法
- (void)setTitle:(NSString *)title {
    _title = title;
    _titleLabel.text = title;
}

- (void)setDesc:(NSString *)desc {
    _desc = desc;
    _descLabel.text = desc;
}

- (void)setImage:(UIImage *)image {
    _image = image;
    _titleImageView.image = image;
}

- (void)setIsLine:(BOOL)isLine {
    _isLine = isLine;
    _line.hidden = !isLine;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(space(31), space(37)));
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleImageView.mas_right).offset(20);
        make.centerY.equalTo(weakSelf.titleImageView.mas_centerY);
    }];
    
    [_indicatorImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(space(14), space(20)));
    }];
    
    [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.indicatorImageView.mas_left).offset(-space(20));
        make.centerY.equalTo(weakSelf.indicatorImageView.mas_centerY);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(1);
    }];
}


@end
