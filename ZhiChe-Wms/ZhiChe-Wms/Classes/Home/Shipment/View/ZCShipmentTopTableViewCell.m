//
//  ZCShipmentTopTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/10.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCShipmentTopTableViewCell.h"

@interface ZCShipmentTopTableViewCell ()

@property (nonatomic, strong) UIView *bigView;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIImageView *bgImageV;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *statusTitleLabel;
@property (nonatomic, strong) UILabel *plateLabel;
@property (nonatomic, strong) UILabel *plateTitleLabel;
@property (nonatomic, strong) UIImageView *carImageV;

@property (nonatomic, strong) UIView *middleView;
@property (nonatomic, strong) UILabel *commandTitleLabel;
@property (nonatomic, strong) UILabel *commandLabel;
@property (nonatomic, strong) UILabel *ownerTitleLabel;
@property (nonatomic, strong) UILabel *ownerLabel;
@property (nonatomic, strong) UILabel *originTitleLabel;
@property (nonatomic, strong) UILabel *originLabel;
@property (nonatomic, strong) UILabel *destTitleLabel;
@property (nonatomic, strong) UILabel *destLabel;
@property (nonatomic, strong) UILabel *styleTitleLabel;
@property (nonatomic, strong) UILabel *styleLabel;
@property (nonatomic, strong) UIImageView *lineImageV;

@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UILabel *driverTitleLabel;
@property (nonatomic, strong) UILabel *driverLabel;

@end

@implementation ZCShipmentTopTableViewCell
#pragma mark - 懒加载
- (UIView *)bigView {
    if (!_bigView) {
        _bigView = [[UIView alloc] init];
        _bigView.backgroundColor = [UIColor whiteColor];
        _bigView.layer.masksToBounds = YES;
        _bigView.layer.cornerRadius = space(6);
    }
    return _bigView;
}

- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = [UIColor whiteColor];
    }
    return _topView;
}

- (UIImageView *)bgImageV {
    if (!_bgImageV) {
        _bgImageV = [[UIImageView alloc] init];
        _bgImageV.image = [UIImage imageNamed:@"shipment_willbg"];
    }
    return _bgImageV;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.textColor = [UIColor whiteColor];
        _statusLabel.font = [UIFont systemFontOfSize:FontSize(36)];
        _statusLabel.text = @"------";
        _statusLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _statusLabel;
}

- (UILabel *)statusTitleLabel {
    if (!_statusTitleLabel) {
        _statusTitleLabel = [[UILabel alloc] init];
        _statusTitleLabel.textColor = ZCColor(0xffffff, 0.8);
        _statusTitleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        _statusTitleLabel.text = @"状态";
        _statusTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _statusTitleLabel;
}

- (UILabel *)plateLabel {
    if (!_plateLabel) {
        _plateLabel = [[UILabel alloc] init];
        _plateLabel.textColor = [UIColor whiteColor];
        _plateLabel.font = [UIFont systemFontOfSize:FontSize(36)];
        _plateLabel.text = @"------";
        _plateLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _plateLabel;
}

- (UILabel *)plateTitleLabel {
    if (!_plateTitleLabel) {
        _plateTitleLabel = [[UILabel alloc] init];
        _plateTitleLabel.textColor = ZCColor(0xffffff, 0.8);
        _plateTitleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        _plateTitleLabel.text = @"车牌号";
        _plateTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _plateTitleLabel;
}

- (UIImageView *)carImageV {
    if (!_carImageV) {
        _carImageV = [[UIImageView alloc] init];
        _carImageV.image = [UIImage imageNamed:@"shipment_car"];
    }
    return _carImageV;
}

- (UIView *)middleView {
    if (!_middleView) {
        _middleView = [[UIView alloc] init];
        _middleView.backgroundColor = [UIColor whiteColor];
    }
    return _middleView;
}

- (UILabel *)commandTitleLabel {
    if (!_commandTitleLabel) {
        _commandTitleLabel = [[UILabel alloc] init];
        _commandTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _commandTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _commandTitleLabel.text = @"指令号";
    }
    return _commandTitleLabel;
}

- (UILabel *)commandLabel {
    if (!_commandLabel) {
        _commandLabel = [[UILabel alloc] init];
        _commandLabel.textColor = ZCColor(0x000000, 0.87);
        _commandLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _commandLabel.text = @"-----";
    }
    return _commandLabel;
}

- (UILabel *)ownerTitleLabel {
    if (!_ownerTitleLabel) {
        _ownerTitleLabel = [[UILabel alloc] init];
        _ownerTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _ownerTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _ownerTitleLabel.text = @"承运商";
    }
    return _ownerTitleLabel;
}

- (UILabel *)ownerLabel {
    if (!_ownerLabel) {
        _ownerLabel = [[UILabel alloc] init];
        _ownerLabel.textColor = ZCColor(0x000000, 0.87);
        _ownerLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _ownerLabel.text = @"----";
    }
    return _ownerLabel;
}

- (UILabel *)originTitleLabel {
    if (!_originTitleLabel) {
        _originTitleLabel = [[UILabel alloc] init];
        _originTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _originTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _originTitleLabel.text = @"起运地";
    }
    return _originTitleLabel;
}

- (UILabel *)originLabel {
    if (!_originLabel) {
        _originLabel = [[UILabel alloc] init];
        _originLabel.textColor = ZCColor(0x000000, 0.87);
        _originLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _originLabel.text = @"----";
    }
    return _originLabel;
}

- (UILabel *)destTitleLabel {
    if (!_destTitleLabel) {
        _destTitleLabel = [[UILabel alloc] init];
        _destTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _destTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _destTitleLabel.text = @"目的地";
    }
    return _destTitleLabel;
}

- (UILabel *)destLabel {
    if (!_destLabel) {
        _destLabel = [[UILabel alloc] init];
        _destLabel.textColor = ZCColor(0x000000, 0.87);
        _destLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _destLabel.text = @"-----";
    }
    return _destLabel;
}

- (UILabel *)styleTitleLabel {
    if (!_styleTitleLabel) {
        _styleTitleLabel = [[UILabel alloc] init];
        _styleTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _styleTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _styleTitleLabel.text = @"运输方式";
    }
    return _styleTitleLabel;
}

- (UILabel *)styleLabel {
    if (!_styleLabel) {
        _styleLabel = [[UILabel alloc] init];
        _styleLabel.textColor = ZCColor(0x000000, 0.87);
        _styleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _styleLabel.text = @"-----";
    }
    return _styleLabel;
}

- (UIImageView *)lineImageV {
    if (!_lineImageV) {
        _lineImageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH - space(80), 2)];
        _lineImageV.image = [self drawLineByImageView:_lineImageV];
    }
    return _lineImageV;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

- (UILabel *)driverTitleLabel {
    if (!_driverTitleLabel) {
        _driverTitleLabel = [[UILabel alloc] init];
        _driverTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _driverTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _driverTitleLabel.text = @"司机";
    }
    return _driverTitleLabel;
}

- (UILabel *)driverLabel {
    if (!_driverLabel) {
        _driverLabel = [[UILabel alloc] init];
        _driverLabel.textColor = ZCColor(0x000000, 0.87);
        _driverLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _driverLabel.text = @"------";
    }
    return _driverLabel;
}

#pragma mark - 画虚线
- (UIImage *)drawLineByImageView:(UIImageView *)imageView{
    UIGraphicsBeginImageContext(imageView.frame.size); //开始画线 划线的frame
    [imageView.image drawInRect:CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height)];
    //设置线条终点形状
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    // 5是每个虚线的长度 1是高度
    CGFloat lengths[] = {5,1};
    CGContextRef line = UIGraphicsGetCurrentContext();
    // 设置颜色
    CGContextSetStrokeColorWithColor(line, [UIColor colorWithWhite:0.408 alpha:1.000].CGColor);
    CGContextSetLineDash(line, 0, lengths, 2); //画虚线
    CGContextMoveToPoint(line, 0.0, 2.0); //开始画线
    CGContextAddLineToPoint(line, SCREENWIDTH - 10, 2.0);
    
    CGContextStrokePath(line);
    // UIGraphicsGetImageFromCurrentImageContext()返回的就是image
    return UIGraphicsGetImageFromCurrentImageContext();
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = ZCColor(0xf7f8fb, 1);
        [self addSubview:self.bigView];
        [self.bigView addSubview:self.topView];
        [self.topView addSubview:self.bgImageV];
        [self.topView addSubview:self.statusLabel];
        [self.topView addSubview:self.statusTitleLabel];
        [self.topView addSubview:self.plateLabel];
        [self.topView addSubview:self.plateTitleLabel];
        [self.topView addSubview:self.carImageV];
        
        [self.bigView addSubview:self.middleView];
        [self.middleView addSubview:self.commandTitleLabel];
        [self.middleView addSubview:self.commandLabel];
        [self.middleView addSubview:self.ownerTitleLabel];
        [self.middleView addSubview:self.ownerLabel];
        [self.middleView addSubview:self.originTitleLabel];
        [self.middleView addSubview:self.originLabel];
        [self.middleView addSubview:self.destTitleLabel];
        [self.middleView addSubview:self.destLabel];
        [self.middleView addSubview:self.styleTitleLabel];
        [self.middleView addSubview:self.styleLabel];
        [self.middleView addSubview:self.lineImageV];
        
        [self.bigView addSubview:self.bottomView];
        [self.bottomView addSubview:self.driverTitleLabel];
        [self.bottomView addSubview:self.driverLabel];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setShipM:(ZCShipmentModel *)shipM {
    _shipM = shipM;
    _statusLabel.text = [shipM.status isEqualToString:@"BS_CREATED"] ? @"已调度" : @"已发运";
    _bgImageV.image = [shipM.status isEqualToString:@"BS_CREATED"] ? [UIImage imageNamed:@"shipment_didbg"] : [UIImage imageNamed:@"shipment_willbg"];
    _plateLabel.text = shipM.plateNo;
    _commandLabel.text = shipM.shipmentGid;
    _ownerLabel.text = shipM.serviceProviderName;
    _originLabel.text = shipM.originLocationName;
    _destLabel.text = shipM.destLocationName;
    _styleLabel.text = shipM.transportModeGid;
    _driverLabel.text = shipM.driverName;
}


#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_bigView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(space(20));
        make.left.equalTo(weakSelf.mas_left).offset(space(20));
        make.right.equalTo(weakSelf.mas_right).offset(-space(20));
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-space(20));
    }];
    
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.bigView.mas_top);
        make.left.equalTo(weakSelf.bigView.mas_left);
        make.right.equalTo(weakSelf.bottomView.mas_right);
        make.height.mas_equalTo(space(184));
    }];
    
    [_bgImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.topView.mas_left);
        make.right.equalTo(weakSelf.topView.mas_right);
        make.top.equalTo(weakSelf.topView.mas_top);
        make.bottom.equalTo(weakSelf.topView.mas_bottom);
    }];
    
    [_statusTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.topView.mas_left).offset((SCREENWIDTH - space(40)) / 4);
        make.top.equalTo(weakSelf.topView.mas_centerY).offset(space(20));
    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.statusTitleLabel.mas_centerX);
        make.bottom.equalTo(weakSelf.topView.mas_centerY);
    }];
    
    [_plateTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.topView.mas_right).offset(-((SCREENWIDTH - space(40)) / 4));
        make.centerY.equalTo(weakSelf.statusTitleLabel.mas_centerY);
    }];
    
    [_plateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.plateTitleLabel.mas_centerX);
        make.centerY.equalTo(weakSelf.statusLabel.mas_centerY);
    }];
    
    [_carImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.topView.mas_centerX);
        make.centerY.equalTo(weakSelf.topView.mas_centerY);
    }];
    
    [_middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.bigView.mas_left);
        make.right.equalTo(weakSelf.bigView.mas_right);
        make.top.equalTo(weakSelf.topView.mas_bottom);
        make.height.mas_equalTo(space(394));
    }];
    
    [_commandTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.middleView.mas_left).offset(space(44));
        make.top.equalTo(weakSelf.middleView.mas_top).offset(space(40));
        make.height.mas_equalTo(space(40));
    }];
    
    [_commandLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.commandTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.middleView.mas_right).offset(-space(44));
        make.left.equalTo(weakSelf.commandTitleLabel.mas_right).offset(space(20));
    }];
    
    [_ownerTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.commandTitleLabel.mas_left);
        make.top.equalTo(weakSelf.commandTitleLabel.mas_bottom).offset(space(30));
        make.height.equalTo(weakSelf.commandTitleLabel.mas_height);
    }];
    
    [_ownerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.ownerTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.commandLabel.mas_right);
        make.left.equalTo(weakSelf.ownerTitleLabel.mas_right).offset(space(20));
    }];
    
    [_originTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.ownerTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.ownerTitleLabel.mas_left);
        make.height.equalTo(weakSelf.ownerTitleLabel.mas_height);
    }];
    
    [_originLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.originTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.ownerLabel.mas_right);
        make.left.equalTo(weakSelf.originTitleLabel.mas_right).offset(space(20));
    }];
    
    [_destTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.originTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.originTitleLabel.mas_left);
        make.height.equalTo(weakSelf.originTitleLabel.mas_height);
    }];
    
    [_destLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.destTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.originLabel.mas_right);
        make.left.equalTo(weakSelf.destTitleLabel.mas_right).offset(space(20));
    }];
    
    [_styleTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.destTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.destTitleLabel.mas_left);
        make.height.equalTo(weakSelf.destTitleLabel.mas_height);
    }];
    
    [_styleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.styleTitleLabel.mas_centerY);
        make.right.equalTo(weakSelf.destLabel.mas_right);
        make.left.equalTo(weakSelf.styleTitleLabel.mas_right).offset(space(20));
    }];
    
    [_lineImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.styleTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.middleView.mas_left).offset(space(20));
        make.right.equalTo(weakSelf.middleView.mas_right).offset(-space(20));
        make.height.mas_equalTo(space(4));
    }];
    
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.middleView.mas_bottom);
        make.left.equalTo(weakSelf.bigView.mas_left);
        make.right.equalTo(weakSelf.bigView.mas_right);
        make.height.mas_equalTo(space(90));
    }];
    
    [_driverTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.styleTitleLabel.mas_left);
        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
    }];
    
    [_driverLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.styleLabel.mas_right);
        make.centerY.equalTo(weakSelf.driverTitleLabel.mas_centerY);
    }];
    
}

@end
