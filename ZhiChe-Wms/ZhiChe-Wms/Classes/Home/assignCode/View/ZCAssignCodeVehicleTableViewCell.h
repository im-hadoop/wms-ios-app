//
//  ZCAssignCodeVehicleTableViewCell.h
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/12.
//  Copyright © 2018 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCAssignCodeVehicleTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) BOOL showLine;

@end

NS_ASSUME_NONNULL_END
