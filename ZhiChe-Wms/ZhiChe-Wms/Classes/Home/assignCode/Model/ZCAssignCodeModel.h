//
//  ZCAssignCodeModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/20.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCAssignCodeModel : NSObject

@property (nonatomic, copy) NSString *token;            //用户token
@property (nonatomic, copy) NSString *whCode;           //仓库code
@property (nonatomic, copy) NSString *userCode;         //用户编码
@property (nonatomic, copy) NSString *originCode;       //起运地编码
@property (nonatomic, copy) NSString *originName;       //起运地名称
@property (nonatomic, copy) NSString *destCode;         //目的地编码
@property (nonatomic, copy) NSString *destName;         //目的地名称
@property (nonatomic, copy) NSString *vin;              //(VIN码)车架号
@property (nonatomic, copy) NSString *orderNo;          //订单号
@property (nonatomic, copy) NSString *wayBillNo;        //运单号
@property (nonatomic, copy) NSString *taskType;         //任务类型(10：寻车，20：移车，30：提车)
@property (nonatomic, copy) NSString *taskStatus;       //任务状态(10：创建，20：开始，30：完成，50：取消)
@property (nonatomic, copy) NSString *orderReleaseGid;  //运单号
@property (nonatomic, copy) NSString *dispatchNo;       //指令号
@property (nonatomic, copy) NSString *taskId;           //任务Id
@property (nonatomic, copy) NSString *vehicle;          //车型
@property (nonatomic, copy) NSString *stanVehicleType;  //标准车型
@property (nonatomic, copy) NSString *otmStatus;
@property (nonatomic, copy) NSString *driverCode;       //司机编码
@property (nonatomic, copy) NSString *driverName;       //司机姓名
@property (nonatomic, copy) NSString *driverPhone;      //司机电话
@property (nonatomic, copy) NSString *startTime;        //开始时间
@property (nonatomic, copy) NSString *finishTime;       //完成时间
@property (nonatomic, copy) NSString *releaseId;        //订单Id
@property (nonatomic, copy) NSString *qrCode;           //二维码

@end

@interface ZCAssignCodeDetailModel : NSObject

@property (nonatomic, copy) NSString *token;           //用户token
@property (nonatomic, copy) NSString *whCode;          //仓库code
@property (nonatomic, copy) NSString *userCode;        //用户编码
@property (nonatomic, copy) NSString *originCode;      //起运地编码
@property (nonatomic, copy) NSString *originName;      //起运地名称
@property (nonatomic, copy) NSString *destCode;        //目的地编码
@property (nonatomic, copy) NSString *destName;        //目的地名称
@property (nonatomic, copy) NSString *vin;             //VIN码(车架号)
@property (nonatomic, copy) NSString *orderNo;         //订单号
@property (nonatomic, copy) NSString *wayBillNo;       //运单号
@property (nonatomic, copy) NSString *taskType;        //任务类型(10：寻车，20：移车，30：提车)
@property (nonatomic, copy) NSString *taskStatus;      //任务状态(10：创建，20：开始，30：完成， 50：取消)
@property (nonatomic, copy) NSString *orderReleaseGid; //系统单号
@property (nonatomic, copy) NSString *dispatchNo;      //指令号
@property (nonatomic, copy) NSString *taskId;          //任务Id
@property (nonatomic, copy) NSString *vehicle;         //车型
@property (nonatomic, copy) NSString *stanVehicleType; //标准车型
@property (nonatomic, copy) NSString *otmStatus;
@property (nonatomic, copy) NSString *driverCode;      //司机编码
@property (nonatomic, copy) NSString *driverName;      //司机名称
@property (nonatomic, copy) NSString *driverPhone;     //司机电话
@property (nonatomic, copy) NSString *startTime;       //开始时间
@property (nonatomic, copy) NSString *finishTime;      //完成时间
@property (nonatomic, copy) NSString *releaseId;       //指令Id
@property (nonatomic, copy) NSString *qrCode;          //二维码
@property (nonatomic, copy) NSString *taskCreate;      //任务创建时间
@property (nonatomic, copy) NSString *taskStart;       //任务开始时间
@property (nonatomic, copy) NSString *taskFinish;      //任务完成时间
@property (nonatomic, copy) NSString *taskNode;        //事件节点
@property (nonatomic, copy) NSString *bindTime;        //二维码绑定时间
@property (nonatomic, copy) NSString *bindId;
@property (nonatomic, copy) NSString *isCanSend;       //是否发运


@end

@interface ZCAssignCodeVehicleInfoModel : NSObject

@property (nonatomic, copy) NSString *vehicleInfoId;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *vehicleType;
@property (nonatomic, copy) NSString *vehicleBrandCode;

@end

@interface ZCAssignCodeVehicleModel : NSObject

@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *pages;
@property (nonatomic, copy) NSString *current;
@property (nonatomic, copy) NSString *searchCount;
@property (nonatomic, copy) NSString *openSort;
@property (nonatomic, copy) NSString *orderByField;
@property (nonatomic, strong) NSArray<ZCAssignCodeVehicleInfoModel *> *records;
@property (nonatomic, copy) NSString *condition;
@property (nonatomic, copy) NSString *asc;
@property (nonatomic, copy) NSString *offsetCurrent;

@end
