//
//  ZCAdjustmentTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/7.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAdjustmentTableViewCell.h"
#import "ZCAdjustmentItemView.h"

@interface ZCAdjustmentTableViewCell ()

@property (nonatomic, strong) ZCAdjustmentItemView *areaView;
@property (nonatomic, strong) ZCAdjustmentItemView *locationView;
@property (nonatomic, strong) ZCAdjustmentItemView *numView;

@end

@implementation ZCAdjustmentTableViewCell
#pragma mark - 懒加载
- (ZCAdjustmentItemView *)areaView {
    if (!_areaView) {
        _areaView = [[ZCAdjustmentItemView alloc] init];
        _areaView.backgroundColor = [UIColor whiteColor];
        _areaView.name = @"库区:";
    }
    return _areaView;
}

- (ZCAdjustmentItemView *)locationView {
    if (!_locationView) {
        _locationView = [[ZCAdjustmentItemView alloc] init];
        _locationView.backgroundColor = [UIColor whiteColor];
        _locationView.name = @"库位:";
    }
    return _locationView;
}

- (ZCAdjustmentItemView *)numView {
    if (!_numView) {
        _numView = [[ZCAdjustmentItemView alloc] init];
        _numView.backgroundColor = [UIColor whiteColor];
        _numView.name = @"存车数量:";
    }
    return _numView;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.areaView];
        [self addSubview:self.locationView];
        [self addSubview:self.numView];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setLocationInfoModel:(ZCLocationInfoModel *)locationInfoModel {
    _locationInfoModel = locationInfoModel;
    self.areaView.content = locationInfoModel.storeAreaName;
    self.locationView.content = locationInfoModel.name;
    self.numView.content = locationInfoModel.maxStorage;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_areaView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(20));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.mas_equalTo((SCREENWIDTH - space(120)) / 3);
    }];
    
    [_locationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.areaView.mas_right).offset(space(20));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(weakSelf.areaView.mas_width);
    }];
    
    [_numView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.locationView.mas_right).offset(space(20));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(weakSelf.locationView.mas_width);
    }];
}

@end
