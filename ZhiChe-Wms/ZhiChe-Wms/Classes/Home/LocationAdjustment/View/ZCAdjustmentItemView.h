//
//  ZCAdjustmentItemView.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/7.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCAdjustmentItemView : UIView

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *content;

@end
