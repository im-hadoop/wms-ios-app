//
//  ZCAdjustmentTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/7.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCAdjustmentModel.h"

@interface ZCAdjustmentTableViewCell : UITableViewCell

@property (nonatomic, strong) ZCLocationInfoModel *locationInfoModel;

@end
