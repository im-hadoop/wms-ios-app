//
//  ZCLoginViewController.m
//  kangbs
//
//  Created by 高睿婕 on 2018/8/13.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCLoginViewController.h"
#import "ZCLoginView.h"
#import "ZCUserTokenModel.h"
#import "ZCHomeTabBarController.h"


@interface ZCLoginViewController ()<ZCLoginViewDelegate>
@property (nonatomic, strong) ZCLoginView *loginView;
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, strong) MBProgressHUD *hud;
@end

@implementation ZCLoginViewController
#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.loginView = [[ZCLoginView alloc] initWithFrame:self.view.bounds];
    self.loginView.delegate = self;
    __weak typeof(self)weakSelf = self;
    self.loginView.accountBlock = ^(NSString *account) {
        weakSelf.account = account;
    };
    self.loginView.passwordBlock = ^(NSString *password) {
        weakSelf.password = password;
    };
    [self.view addSubview:self.loginView];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
}

#pragma mark - ZCLoginViewDelegate
- (void)ZCLoginViewLoginButtonAction {
    if (self.account.length == 0) {
        NSLog(@"请输入用户名");
    }else if (self.password.length == 0) {
        NSLog(@"请输入密码");
    }else {
        [self.view endEditing:YES];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:self.account forKey:@"username"];
        [params setObject:self.password forKey:@"password"];
        [params setObject:@"devops" forKey:@"client_id"];
        [params setObject:@"devops" forKey:@"client_secret"];
        [params setObject:@"password" forKey:@"grant_type"];
        self.hud.mode = MBProgressHUDModeIndeterminate;
        self.hud.label.text = nil;
        [self.hud showAnimated:YES];
        __weak typeof(self)weakSelf = self;
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [ZCHttpTool getRefreshTokenWithURL:UserToken params:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                ZCUserTokenModel *userTokenModel = [ZCUserTokenModel yy_modelWithDictionary:responseObject];
                if (userTokenModel.access_token.length > 0) {
                    [weakSelf.hud hideAnimated:YES];
                    [[NSUserDefaults standardUserDefaults] setObject:userTokenModel.access_token forKey:loginToken];
                    [[NSUserDefaults standardUserDefaults] setObject:userTokenModel.refresh_token forKey:refreshToken];
                    [[NSUserDefaults standardUserDefaults] setObject:userTokenModel.expires_in forKey:effectiveTime];
                    [[NSUserDefaults standardUserDefaults] setObject:userTokenModel.gmt_create forKey:tokenCreateTime];
                    [[NSUserDefaults standardUserDefaults] setObject:userTokenModel.accountId forKey:AccountId];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    ZCHomeTabBarController *homeTabVC = [[ZCHomeTabBarController alloc] init];
                    [UIApplication sharedApplication].keyWindow.rootViewController = homeTabVC;
                }else {
                    weakSelf.hud.mode = MBProgressHUDModeText;
                    weakSelf.hud.label.text = responseObject[@"message"];
                    [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
                }
            } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = error.localizedDescription;
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }];

        });
       
    }
    
    
}


@end
