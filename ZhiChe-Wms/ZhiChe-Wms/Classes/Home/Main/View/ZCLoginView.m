//
//  ZCLoginView.m
//  kangbs
//
//  Created by 高睿婕 on 2018/8/13.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCLoginView.h"

@interface ZCLoginView ()<UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *titleImageView;
@property (nonatomic, strong) UILabel *accountLabel;
@property (nonatomic, strong) UILabel *passwordLabel;
@property (nonatomic, strong) UITextField *accountTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UIView *accountLine;
@property (nonatomic, strong) UIView *passwordLine;
@property (nonatomic, strong) UIButton *loginButton;
@property (nonatomic, strong) UILabel *tipLabel;
@property (nonatomic, strong) UIButton *allowButton;
@property (nonatomic, strong) UILabel *allowLabel;
@end

@implementation ZCLoginView
#pragma mark - 懒加载
- (UIImageView *)titleImageView {
    if (!_titleImageView) {
        _titleImageView = [[UIImageView alloc] init];
        _titleImageView.image = [UIImage imageNamed:@"login-logo"];
    }
    return _titleImageView;
}

- (UILabel *)accountLabel {
    if (!_accountLabel) {
        _accountLabel = [[UILabel alloc] init];
        _accountLabel.text = @"账号";
        _accountLabel.font = [UIFont systemFontOfSize:FontSize(32)];
        _accountLabel.textColor = ZCColor(0x000000, 0.87);
    }
    return _accountLabel;
}

- (UILabel *)passwordLabel {
    if (!_passwordLabel) {
        _passwordLabel = [[UILabel alloc] init];
        _passwordLabel.text = @"密码";
        _passwordLabel.font = [UIFont systemFontOfSize:FontSize(32)];
        _passwordLabel.textColor = ZCColor(0x000000, 0.87);
    }
    return _passwordLabel;
}

- (UITextField *)accountTextField {
    if (!_accountTextField) {
        _accountTextField = [[UITextField alloc] init];
        _accountTextField.placeholder = @"请输入用户名";
        _accountTextField.delegate = self;
        _accountTextField.keyboardType = UIKeyboardTypeASCIICapable;
        [_accountTextField addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _accountTextField;
}

- (UITextField *)passwordTextField {
    if (!_passwordTextField) {
        _passwordTextField = [[UITextField alloc] init];
        _passwordTextField.placeholder = @"请输入密码";
        _passwordTextField.delegate = self;
        _passwordTextField.keyboardType = UIKeyboardTypeASCIICapable;
        _passwordTextField.secureTextEntry = YES;
        [_passwordTextField addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _passwordTextField;
}

- (UIView *)accountLine {
    if (!_accountLine) {
        _accountLine = [[UIView alloc] init];
        _accountLine.backgroundColor = ZCColor(0x000000, 0.1);
    }
    return _accountLine;
}

- (UIView *)passwordLine {
    if (!_passwordLine) {
        _passwordLine = [[UIView alloc] init];
        _passwordLine.backgroundColor = ZCColor(0x000000, 0.1);
        
    }
    return _passwordLine;
}

- (UIButton *)loginButton {
    if (!_loginButton) {
        _loginButton = [[UIButton alloc] init];
        [_loginButton setBackgroundColor:ZCColor(0xff8213, 0.5)];
        [_loginButton setTitle:@"登录" forState:UIControlStateNormal];
        [_loginButton setTitleColor:ZCColor(0xffffff, 1) forState:UIControlStateNormal];
        _loginButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(36)];
        _loginButton.layer.masksToBounds = YES;
        _loginButton.layer.cornerRadius = space(6);
        [_loginButton addTarget:self action:@selector(loginButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        _loginButton.enabled = NO;
    }
    return _loginButton;
}

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.textColor = ZCColor(0xa1a1a1, 1);
        _tipLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:@"授权康舶司现场版应用访问你的以下权限"];
        [attr addAttribute:NSForegroundColorAttributeName value:ZCColor(0xff8213, 1) range:NSMakeRange(2, 6)];
//        [attr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontSize(26)] range:NSMakeRange(0, attr.length)];
        _tipLabel.attributedText = attr;
    }
    return _tipLabel;
}

- (UIButton *)allowButton {
    if (!_allowButton) {
        _allowButton = [[UIButton alloc] init];
        [_allowButton setImage:[UIImage imageNamed:@"login_normal"] forState:UIControlStateNormal];
        [_allowButton setImage:[UIImage imageNamed:@"login_select"] forState:UIControlStateSelected];
        [_allowButton addTarget:self action:@selector(allowButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _allowButton;
}

- (UILabel *)allowLabel {
    if (!_allowLabel) {
        _allowLabel = [[UILabel alloc] init];
        _allowLabel.text = @"获取你的账号信息";
        _allowLabel.textColor = ZCColor(0x000000, 0.34);
        _allowLabel.font = [UIFont systemFontOfSize:FontSize(24)];
    }
    return _allowLabel;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self addSubview:self.titleImageView];
        [self addSubview:self.accountLabel];
        [self addSubview:self.accountTextField];
        [self addSubview:self.accountLine];
        [self addSubview:self.passwordLabel];
        [self addSubview:self.passwordTextField];
        [self addSubview:self.passwordLine];
        [self addSubview:self.loginButton];
//        [self addSubview:self.tipLabel];
//        [self addSubview:self.allowButton];
//        [self addSubview:self.allowLabel];
    }
    return self;
}

#pragma mark - UITextField 触发
- (void)textFieldChange:(UITextField *)textField {
    if (textField == _accountTextField) {
        if (_accountBlock) {
            _accountBlock(textField.text);
        }
    }else if (textField == _passwordTextField){
        if (_passwordBlock) {
            _passwordBlock(textField.text);
        }
    }
    if (self.accountTextField.text.length > 0 && self.passwordTextField.text.length > 0) {
        self.loginButton.backgroundColor = ZCColor(0xff8213, 1);
        self.loginButton.enabled = YES;
    }else {
        self.loginButton.backgroundColor = ZCColor(0xff8213, 0.5);
        self.loginButton.enabled = NO;
    }
}

#pragma mark - 按钮点击
- (void)loginButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(ZCLoginViewLoginButtonAction)]) {
        [self.delegate ZCLoginViewLoginButtonAction];
    }
}

- (void)allowButtonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField becomeFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(space(140));
        make.centerX.equalTo(weakSelf.mas_centerX);
    }];
    
    [_accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.titleImageView.mas_bottom).offset(space(106));
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
    }];
    
    [_accountTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.accountLabel.mas_centerY);
        make.left.equalTo(weakSelf.accountLabel.mas_right).offset(space(70));
        make.width.mas_equalTo(space(460));
        make.height.mas_equalTo(space(90));
    }];
    
    [_accountLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.accountLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.accountLabel.mas_left);
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.height.mas_equalTo(space(2));
    }];
    
    [_passwordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.accountLine.mas_bottom).offset(space(50));
        make.left.equalTo(weakSelf.accountLine.mas_left);
    }];
    
    [_passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.passwordLabel.mas_centerY);
        make.left.equalTo(weakSelf.accountTextField.mas_left);
        make.width.mas_equalTo(space(460));
        make.height.mas_equalTo(space(90));
    }];
    
    [_passwordLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.passwordLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.accountLine.mas_left);
        make.right.equalTo(weakSelf.accountLine.mas_right);
        make.height.equalTo(weakSelf.accountLine.mas_height);
    }];
    
    [_loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.passwordLine.mas_bottom).offset(space(40));
        make.left.equalTo(weakSelf.passwordLine.mas_left);
        make.right.equalTo(weakSelf.passwordLine.mas_right);
        make.height.mas_equalTo(space(88));
    }];
    
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.loginButton.mas_bottom).offset(space(40));
        make.left.equalTo(weakSelf.loginButton.mas_left);
    }];
    
    [_allowButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.tipLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.tipLabel.mas_left);
    }];
    
    [_allowLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.allowButton.mas_right).offset(space(14));
        make.centerY.equalTo(weakSelf.allowButton.mas_centerY);
    }];
    
}

@end
