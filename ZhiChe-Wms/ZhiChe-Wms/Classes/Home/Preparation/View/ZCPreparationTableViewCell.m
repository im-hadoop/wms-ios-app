//
//  ZCPreparationTableViewCell.m
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/14.
//  Copyright © 2018 Regina. All rights reserved.
//

#import "ZCPreparationTableViewCell.h"
#import "ZCPreparationModel.h"

@interface ZCPreparationTableViewCell ()

@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *orderNo;
@property (nonatomic, strong) UILabel *orderNoContentLabel;
@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, strong) UIView *middleView;
@property (nonatomic, strong) UILabel *ownerLabel;
@property (nonatomic, strong) UILabel *ownerContentLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *timeContentLabel;
@property (nonatomic, strong) UILabel *carWayLabel;
@property (nonatomic, strong) UILabel *carWayContentLabel;
@property (nonatomic, strong) UILabel *numsLabel;
@property (nonatomic, strong) UIView *middleLine;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UILabel *carNumLabel;
@property (nonatomic, strong) UILabel *carNumContentLabel;
@property (nonatomic, strong) UIView *bottomLine;

@end

@implementation ZCPreparationTableViewCell
#pragma mark - 懒加载
- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = [UIColor whiteColor];
    }
    return _topView;
}

- (UILabel *)orderNo {
    if (!_orderNo) {
        _orderNo = [[UILabel alloc] init];
        _orderNo.font = [UIFont boldSystemFontOfSize:FontSize(28)];
        _orderNo.textColor = ZCColor(0x000000, 0.87);
        _orderNo.text = @"指令号";
//        _orderNo.backgroundColor = [UIColor redColor];
//        _orderNo.textAlignment = NSTextAlignmentCenter;
    }
    return _orderNo;
}

- (UILabel *)orderNoContentLabel {
    if (!_orderNoContentLabel) {
        _orderNoContentLabel = [[UILabel alloc] init];
        _orderNoContentLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
        _orderNoContentLabel.textColor = ZCColor(0x000000, 0.87);
        _orderNoContentLabel.text = @"122344555";
//        _orderNoContentLabel.backgroundColor = [UIColor greenColor];
    }
    return _orderNoContentLabel;
}

- (UIView *)topLine {
    if (!_topLine) {
        _topLine = [[UIView alloc] init];
        _topLine.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _topLine;
}

- (UIView *)middleView {
    if (!_middleView) {
        _middleView = [[UIView alloc] init];
        _middleView.backgroundColor = [UIColor whiteColor];
    }
    return _middleView;
}

- (UILabel *)ownerLabel {
    if (!_ownerLabel) {
        _ownerLabel = [[UILabel alloc] init];
        _ownerLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _ownerLabel.textColor = ZCColor(0x000000, 0.34);
        _ownerLabel.text = @"承运商";
//        _ownerLabel.backgroundColor = [UIColor redColor];
//        _ownerLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _ownerLabel;
}

- (UILabel *)ownerContentLabel {
    if (!_ownerContentLabel) {
        _ownerContentLabel = [[UILabel alloc] init];
        _ownerContentLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _ownerContentLabel.textColor = ZCColor(0x000000, 0.56);
        _ownerContentLabel.text = @"江铃南昌股份有限责任公司";
        _ownerContentLabel.numberOfLines = 0;
    }
    return _ownerContentLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _timeLabel.textColor = ZCColor(0x000000, 0.34);
        _timeLabel.text = @"计划装车时间";
//        _timeLabel.backgroundColor = [UIColor yellowColor];
//        _timeLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _timeLabel;
}

- (UILabel *)timeContentLabel {
    if (!_timeContentLabel) {
        _timeContentLabel = [[UILabel alloc] init];
        _timeContentLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _timeContentLabel.textColor = ZCColor(0x000000, 0.56);
        _timeContentLabel.text = @"2018-1-1";
//        _timeContentLabel.backgroundColor = [UIColor yellowColor];
    }
    return _timeContentLabel;
}

- (UILabel *)carWayLabel {
    if (!_carWayLabel) {
        _carWayLabel = [[UILabel alloc] init];
        _carWayLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _carWayLabel.textColor = ZCColor(0x000000, 0.34);
        _carWayLabel.text = @"备车道";
//        _carWayLabel.backgroundColor = [UIColor greenColor];
//        _carWayLabel.textAlignment = NSTextAlignmentRight;
        _carWayLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _carWayLabel;
}

- (UILabel *)carWayContentLabel {
    if (!_carWayContentLabel) {
        _carWayContentLabel = [[UILabel alloc] init];
        _carWayContentLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _carWayContentLabel.textColor = ZCColor(0x000000, 0.56);
        _carWayContentLabel.text = @"11车道";
    }
    return _carWayContentLabel;
}

- (UILabel *)numsLabel {
    if (!_numsLabel) {
        _numsLabel = [[UILabel alloc] init];
        _numsLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _numsLabel.textColor = ZCColor(0xff8213, 1);
        _numsLabel.text = @"几台车";
//        _numsLabel.backgroundColor = [UIColor blueColor];
//        _numsLabel.textAlignment = NSTextAlignmentRight;
    }
    return _numsLabel;
}

- (UIView *)middleLine {
    if (!_middleLine) {
        _middleLine = [[UIView alloc] init];
        _middleLine.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _middleLine;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

- (UILabel *)carNumLabel {
    if (!_carNumLabel) {
        _carNumLabel = [[UILabel alloc] init];
        _carNumLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
        _carNumLabel.textColor = ZCColor(0x000000, 0.87);
        _carNumLabel.text = @"板车号";
        _carNumLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _carNumLabel;
}

- (UILabel *)carNumContentLabel {
    if (!_carNumContentLabel) {
        _carNumContentLabel = [[UILabel alloc] init];
        _carNumContentLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
        _carNumContentLabel.textColor = ZCColor(0x000000, 0.87);
        _carNumContentLabel.text = @"129djhfj9877";
    }
    return _carNumContentLabel;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc] init];
        _bottomLine.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _bottomLine;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.topView];
        [self.topView addSubview:self.orderNo];
        [self.topView addSubview:self.orderNoContentLabel];
        [self.topView addSubview:self.topLine];
        [self addSubview:self.middleView];
        [self.middleView addSubview:self.ownerLabel];
        [self.middleView addSubview:self.ownerContentLabel];
        [self.middleView addSubview:self.timeLabel];
        [self.middleView addSubview:self.timeContentLabel];
        [self.middleView addSubview:self.carWayLabel];
        [self.middleView addSubview:self.carWayContentLabel];
        [self.middleView addSubview:self.numsLabel];
        [self.middleView addSubview:self.middleLine];
        [self addSubview:self.bottomView];
        [self.bottomView addSubview:self.carNumLabel];
        [self.bottomView addSubview:self.carNumContentLabel];
        [self.bottomView addSubview:self.bottomLine];
    }
    return self;
}

#pragma mark - 属性设置
- (void)setListModel:(ZCPreparationTaskInfoModel *)listModel {
    _listModel = listModel;
    self.orderNoContentLabel.text = listModel.sourceKey;
    self.ownerContentLabel.text = listModel.serviceProviderName;
    self.carWayContentLabel.text = listModel.loadingArea;
    self.timeContentLabel.text = listModel.planTime;
    self.numsLabel.text = [NSString stringWithFormat:@"%@台车",listModel.planSumQty];
    self.carNumContentLabel.text = listModel.plateNumber;
    
    NSMutableDictionary *attr = [NSMutableDictionary dictionary];
    [attr setObject:[UIFont systemFontOfSize:FontSize(28)] forKey:NSFontAttributeName];
    CGSize carWaySize = [self.carWayContentLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    __weak typeof(self)weakSelf = self;
    [self.carWayContentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.middleView.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.ownerLabel.mas_centerY);
        make.width.mas_equalTo(carWaySize.width + space(20));
    }];
    
    CGSize carWSize = [self.carWayLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    [self.carWayLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.carWayContentLabel.mas_left).offset(-space(10));
        make.centerY.equalTo(weakSelf.carWayContentLabel.mas_centerY);
        make.width.mas_equalTo(carWSize.width + space(10));
    }];
    
    CGSize numsSize = [self.numsLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    [self.numsLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.middleView.mas_right).offset(-space(40));
        make.top.equalTo(weakSelf.carWayLabel.mas_bottom).offset(space(40));
        make.width.mas_equalTo(numsSize.width + space(20));
    }];
    
    NSLog(@"属性设置");
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    NSLog(@"布局");
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(100));
    }];
    
    NSMutableDictionary *titleAttr = [NSMutableDictionary dictionary];
    [titleAttr setObject:[UIFont boldSystemFontOfSize:FontSize(28)] forKey:NSFontAttributeName];
    
    CGSize orderSize = [self.orderNo.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:titleAttr context:nil].size;
    [self.orderNo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.topView.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.topView.mas_centerY);
        make.width.mas_equalTo(orderSize.width + space(10));
    }];
    
    [self.orderNoContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.orderNo.mas_right).offset(space(20));
        make.centerY.equalTo(weakSelf.orderNo.mas_centerY);
        make.right.equalTo(weakSelf.topView.mas_right).offset(-space(40));
    }];
    
    [self.topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.topView.mas_left);
        make.right.equalTo(weakSelf.topView.mas_right);
        make.bottom.equalTo(weakSelf.topView.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    [self.middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.topView.mas_bottom);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(200));
    }];
    
    NSMutableDictionary *attr = [NSMutableDictionary dictionary];
    [attr setObject:[UIFont systemFontOfSize:FontSize(28)] forKey:NSFontAttributeName];
    CGSize sizeOwner = [self.ownerLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, space(60)) options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    [self.ownerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.middleView.mas_left).offset(space(40));
        make.top.equalTo(weakSelf.middleView.mas_top).offset(space(40));
        make.width.mas_equalTo(sizeOwner.width + space(10));
    }];
    
    [self.ownerContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.ownerLabel.mas_right).offset(space(10));
        make.centerY.equalTo(weakSelf.ownerLabel.mas_centerY);
        make.right.equalTo(weakSelf.carWayLabel.mas_left).offset(-space(20));
    }];
    
    CGSize timeSize = [self.timeLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.ownerLabel.mas_left);
        make.top.equalTo(weakSelf.ownerLabel.mas_bottom).offset(space(40));
        make.width.mas_equalTo(timeSize.width + space(10));
    }];
    
    [self.timeContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.timeLabel.mas_right).offset(space(10));
        make.centerY.equalTo(weakSelf.timeLabel.mas_centerY);
        make.right.equalTo(weakSelf.numsLabel.mas_left).offset(-space(20));
    }];
    
    [self.middleLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.middleView.mas_left);
        make.right.equalTo(weakSelf.middleView.mas_right);
        make.bottom.equalTo(weakSelf.middleView.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.middleView.mas_bottom);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(100));
    }];
    
    CGSize carNumSize = [self.carNumLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:titleAttr context:nil].size;
    [self.carNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.bottomView.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
        make.width.mas_equalTo(carNumSize.width + space(10));
    }];
    
    [self.carNumContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.carNumLabel.mas_right).offset(space(10));
        make.centerY.equalTo(weakSelf.carNumLabel.mas_centerY);
        make.right.equalTo(weakSelf.bottomView.mas_right).offset(-space(40));
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.bottomView.mas_left);
        make.right.equalTo(weakSelf.bottomView.mas_right);
        make.bottom.equalTo(weakSelf.bottomView.mas_bottom);
        make.height.mas_equalTo(1);
    }];
}

@end
