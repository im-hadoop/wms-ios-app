//
//  ZCPreparationTableViewCell.h
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/14.
//  Copyright © 2018 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZCPreparationTaskInfoModel;

NS_ASSUME_NONNULL_BEGIN

@interface ZCPreparationTableViewCell : UITableViewCell

@property (nonatomic, strong) ZCPreparationTaskInfoModel *listModel;

@end

NS_ASSUME_NONNULL_END
