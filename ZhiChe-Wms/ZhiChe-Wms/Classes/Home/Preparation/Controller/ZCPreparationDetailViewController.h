//
//  ZCPreparationDetailViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/6.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCPreparationDetailViewController : UIViewController

@property (nonatomic, copy) NSString *houseId;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *visitType;

@end
