//
//  ZCPreparationModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/6.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCPreparationModel : NSObject

@property (nonatomic, copy) NSString *Id;                  //Id
@property (nonatomic, copy) NSString *headerId;            //备料单头键
@property (nonatomic, copy) NSString *noticeLineId;        //通知单明细键
@property (nonatomic, copy) NSString *locationId;          //储位
@property (nonatomic, copy) NSString *stockId;             //库存Id
@property (nonatomic, copy) NSString *locationNo;          //储位号
@property (nonatomic, copy) NSString *shipSpace;           //发货区
@property (nonatomic, copy) NSString *ownerId;             //货主
@property (nonatomic, copy) NSString *ownerOrderNo;        //货主订单号
@property (nonatomic, copy) NSString *lgsOrderNo;          //物流订单号
@property (nonatomic, copy) NSString *materielId;          //物料Id
@property (nonatomic, copy) NSString *materielCode;        //物料编码
@property (nonatomic, copy) NSString *materielName;        //物料名称
@property (nonatomic, copy) NSString *uom;                 //计量单位
@property (nonatomic, copy) NSString *planQty;             //计划数量
@property (nonatomic, copy) NSString *planNetWeight;       //计划净重
@property (nonatomic, copy) NSString *planGrossWeight;     //计划毛重
@property (nonatomic, copy) NSString *planGrossCubage;     //计划体积
@property (nonatomic, copy) NSString *planPackedCount;     //计划件数
@property (nonatomic, copy) NSString *actualQty;           //实际数量
@property (nonatomic, copy) NSString *actualNetWeight;     //实际净重
@property (nonatomic, copy) NSString *actualGrossWeight;   //实际毛重
@property (nonatomic, copy) NSString *actualGrossCubage;   //实际体积
@property (nonatomic, copy) NSString *actualPackedCount;   //实际件量
@property (nonatomic, copy) NSString *preparetor;          //备料人
@property (nonatomic, copy) NSString *planTime;            //计划时间
@property (nonatomic, copy) NSString *startTime;           //开始时间
@property (nonatomic, copy) NSString *finishTime;          //完成时间
@property (nonatomic, copy) NSString *lotNo0;              //批号0
@property (nonatomic, copy) NSString *lotNo1;              //批号1（车架号）
@property (nonatomic, copy) NSString *lotNo2;              //批号2
@property (nonatomic, copy) NSString *lotNo3;              //批号3
@property (nonatomic, copy) NSString *lotNo4;              //批号4
@property (nonatomic, copy) NSString *lotNo5;              //批号5
@property (nonatomic, copy) NSString *lotNo6;              //批号6
@property (nonatomic, copy) NSString *lotNo7;              //批号7
@property (nonatomic, copy) NSString *lotNo8;              //批号8
@property (nonatomic, copy) NSString *lotNo9;              //批号9
@property (nonatomic, copy) NSString *status;              //状态(10：未开始，20：已开始，30：已完成)
@property (nonatomic, copy) NSString *remarks;             //备注
@property (nonatomic, copy) NSString *gmtCreate;           //创建时间
@property (nonatomic, copy) NSString *gmtModified;         //修改时间
@property (nonatomic, copy) NSString *storeHouseId;        //收货仓库Id
@property (nonatomic, copy) NSString *storeHouseName;      //收货仓库名称
@property (nonatomic, copy) NSString *ownerName;           //货主名称
@property (nonatomic, copy) NSString *carrierId;           //承运商
@property (nonatomic, copy) NSString *carrierName;         //承运商名称
@property (nonatomic, copy) NSString *plateNumber;         //车船号
@property (nonatomic, copy) NSString *recvDate;            //单据日期
@property (nonatomic, copy) NSString *noticeNo;            //通知单号
@property (nonatomic, copy) NSString *vehicleCheckoutStatus;  //实车校验状态
@property (nonatomic, copy) NSString *isCanSend;           //是否发运
@property (nonatomic, copy) NSString *userModified;        //修改人
@property (nonatomic, copy) NSString *storeDetail;         //车辆位置、库位
@property (nonatomic, copy) NSString *qrCode;              //二维码

@end


@interface ZCPreparationTaskInfoModel : NSObject

@property (nonatomic, copy) NSString *taskInfoId;
@property (nonatomic, copy) NSString *noticeId;
@property (nonatomic, copy) NSString *prepareNo;
@property (nonatomic, copy) NSString *orderTime;
@property (nonatomic, copy) NSString *planTime;
@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *finishTime;
@property (nonatomic, copy) NSString *carrierId;
@property (nonatomic, copy) NSString *carrierName;
@property (nonatomic, copy) NSString *transportMethod;
@property (nonatomic, copy) NSString *plateNumber;
@property (nonatomic, copy) NSString *driverName;
@property (nonatomic, copy) NSString *driverPhone;
@property (nonatomic, copy) NSString *uom;
@property (nonatomic, copy) NSString *planSumQty;
@property (nonatomic, copy) NSString *actualSumQty;
@property (nonatomic, copy) NSString *lineCount;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *remarks;
@property (nonatomic, copy) NSString *userCreate;
@property (nonatomic, copy) NSString *userModified;
@property (nonatomic, copy) NSString *gmtCreate;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, copy) NSString *loadingArea;
@property (nonatomic, copy) NSString *serviceProviderName;
@property (nonatomic, copy) NSString *sourceKey;

@end

@interface ZCPreparationListModel : NSObject

@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *pages;
@property (nonatomic, copy) NSString *current;
@property (nonatomic, copy) NSString *searchCount;
@property (nonatomic, copy) NSString *openSort;
@property (nonatomic, copy) NSString *orderByField;
@property (nonatomic, strong) NSArray<ZCPreparationTaskInfoModel *> *records;
@end


