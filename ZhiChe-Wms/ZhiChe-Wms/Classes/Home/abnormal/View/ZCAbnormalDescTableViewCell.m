//
//  ZCAbnormalDescTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalDescTableViewCell.h"


@interface ZCAbnormalDescTableViewCell ()
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIButton *cameraButton;
@property (nonatomic, strong) UIView *lineView;
@end

@implementation ZCAbnormalDescTableViewCell
#pragma mark - 懒加载
- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [[UILabel alloc] init];
        _descLabel.text = @"划伤";
        _descLabel.textColor = ZCColor(0x000000, 0.54);
        _descLabel.font = [UIFont systemFontOfSize:FontSize(26)];
    }
    return _descLabel;
}

- (UIButton *)cameraButton {
    if (!_cameraButton) {
        _cameraButton = [[UIButton alloc] init];
        [_cameraButton setImage:[UIImage imageNamed:@"camera_normal"] forState:UIControlStateNormal];
        [_cameraButton setImage:[UIImage imageNamed:@"camera_heighlight"] forState:UIControlStateHighlighted];
        [_cameraButton addTarget:self action:@selector(cameraButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cameraButton;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

#pragma mark - 初始化方法
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = ZCColor(0xf5f9fd, 1);
        [self addSubview:self.descLabel];
        [self addSubview:self.cameraButton];
        [self addSubview:self.lineView];
    }
    return self;
}

#pragma mark - 按钮点击
- (void)cameraButtonClick:(UIButton *)sender {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    CGRect rect1 = [sender convertRect:sender.frame fromView:self.contentView];
    CGRect rect2 = [sender convertRect:rect1 toView:window];
    if ([self.delegate respondsToSelector:@selector(abnormalDescTableViewCellCameraButtonClickWithIndex:rectInScreen:)]) {
        [self.delegate abnormalDescTableViewCellCameraButtonClickWithIndex:self.index rectInScreen:rect2];
    }
}

#pragma mark - 属性方法
- (void)setDesc:(NSString *)desc {
    _desc = desc;
    self.descLabel.text = desc;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    
    [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(50));
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_cameraButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(50));
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(space(2));
    }];
}

@end
