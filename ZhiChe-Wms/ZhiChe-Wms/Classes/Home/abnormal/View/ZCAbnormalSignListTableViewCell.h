//
//  ZCAbnormalSignListTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/22.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZCAbnormalSignListTableViewCellDelegate <NSObject>

- (void)abnormalSignListTableViewCellDeleteButtonClickWithIndex:(NSIndexPath *)index;
- (void)abnormalSignListTableViewCellScanPhotoWithCellIndex:(NSIndexPath *)cellIndex itemIndex:(NSIndexPath *)itemIndex;
- (void)abnormalSignListTableViewCellLongPressImageWithCellIndex:(NSIndexPath *)cellIndex itemIndex:(NSIndexPath *)itemIndex;

@end

@interface ZCAbnormalSignListTableViewCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) id<ZCAbnormalSignListTableViewCellDelegate> delegate;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, assign) BOOL isDelete;

@end
