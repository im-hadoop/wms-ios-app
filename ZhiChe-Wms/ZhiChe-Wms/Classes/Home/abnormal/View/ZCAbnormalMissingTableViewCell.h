//
//  ZCAbnormalMissingTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCAbnormalMissingTableViewCell : UITableViewCell
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) UIColor *contentColor;
@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, assign) BOOL canSelected;
@end
