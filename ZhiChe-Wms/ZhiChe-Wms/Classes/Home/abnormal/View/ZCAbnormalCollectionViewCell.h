//
//  ZCAbnormalCollectionViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCAbnormalCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) NSString *num;
@property (nonatomic, assign) BOOL isTopLine;
@property (nonatomic, assign) BOOL isBottomLine;
@property (nonatomic, assign) BOOL isLeftLine;
@property (nonatomic, assign) BOOL isRightLine;

@end
