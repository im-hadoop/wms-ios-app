//
//  ZCAbnormalCollectionViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalCollectionViewCell.h"

@interface ZCAbnormalCollectionViewCell ()

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, strong) UIView *botttomLine;
@property (nonatomic, strong) UIView *leftLine;
@property (nonatomic, strong) UIView *rightLine;

@end

@implementation ZCAbnormalCollectionViewCell
#pragma mark - 懒加载
- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    }
    return _iconImageView;
}

- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _numLabel.textColor = [UIColor whiteColor];
        _numLabel.font = [UIFont systemFontOfSize:FontSize(40)];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.backgroundColor = ZCColor(0xff0033, 0.5);
        _numLabel.hidden = YES;
    }
    return _numLabel;
}

- (UIView *)topLine {
    if (!_topLine) {
        _topLine = [[UIView alloc] init];
        _topLine.backgroundColor = [UIColor blueColor];
    }
    return _topLine;
}

- (UIView *)botttomLine {
    if (!_botttomLine) {
        _botttomLine = [[UIView alloc] init];
        _botttomLine.backgroundColor = [UIColor blueColor];
    }
    return _botttomLine;
}

- (UIView *)leftLine {
    if (!_leftLine) {
        _leftLine = [[UIView alloc] init];
        _leftLine.backgroundColor = [UIColor blueColor];
    }
    return _leftLine;
}

- (UIView *)rightLine {
    if (!_rightLine) {
        _rightLine = [[UIView alloc] init];
        _rightLine.backgroundColor = [UIColor blueColor];
    }
    return _rightLine;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.iconImageView];
        [self addSubview:self.numLabel];
        [self addSubview:self.topLine];
        [self addSubview:self.botttomLine];
        [self addSubview:self.leftLine];
        [self addSubview:self.rightLine];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setImage:(UIImage *)image {
    _image = image;
    _iconImageView.image = image;
}

- (void)setNum:(NSString *)num {
    _num = num;
    _numLabel.text = num;
    if ([num integerValue] == 0) {
        _numLabel.hidden = YES;
    }else {
        _numLabel.hidden = NO;
    }
}

- (void)setIsTopLine:(BOOL)isTopLine {
    _isTopLine = isTopLine;
    self.topLine.hidden = !isTopLine;
}

- (void)setIsBottomLine:(BOOL)isBottomLine {
    _isBottomLine = isBottomLine;
    self.botttomLine.hidden = !isBottomLine;
}

- (void)setIsLeftLine:(BOOL)isLeftLine {
    _isLeftLine = isLeftLine;
    self.leftLine.hidden = !isLeftLine;
}

- (void)setIsRightLine:(BOOL)isRightLine {
    _isRightLine = isRightLine;
    self.rightLine.hidden = !isRightLine;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(2));
    }];
    
    [_botttomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(2));
    }];
    
    [_leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.topLine.mas_bottom);
        make.bottom.equalTo(weakSelf.botttomLine.mas_top);
        make.left.equalTo(weakSelf.mas_left);
        make.width.mas_equalTo(space(2));
    }];
    
    [_rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.leftLine.mas_top);
        make.bottom.equalTo(weakSelf.leftLine.mas_bottom);
        make.right.equalTo(weakSelf.mas_right);
        make.width.mas_equalTo(space(2));
    }];
}

@end
