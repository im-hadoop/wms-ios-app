//
//  ZCAbnormalSignView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalSignView.h"
#import "ZCAbnormalCollectionViewCell.h"
#import "ZCAbnormalModel.h"


static NSString *signCell = @"ZCAbnormalCollectionViewCell";

@interface ZCAbnormalSignView ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation ZCAbnormalSignView
#pragma mark - 懒加载
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[ZCAbnormalCollectionViewCell class] forCellWithReuseIdentifier:signCell];
        _collectionView.scrollEnabled = NO;
    }
    return _collectionView;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.collectionView];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setCloakArray:(NSArray *)cloakArray {
    _cloakArray = cloakArray;
    [self.collectionView reloadData];
}

- (void)setSignArray:(NSArray *)signArray {
    _signArray = signArray;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 9;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZCAbnormalCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:signCell forIndexPath:indexPath];
    cell.image = [UIImage imageNamed:self.imageArray[indexPath.item]];
    if (!self.cloack) {
        ZCAbnormalInfoModel *infoM = (ZCAbnormalInfoModel *)self.signArray[indexPath.item];
        cell.num = infoM.countExcp;
        if (indexPath.item == 3) {
            cell.isTopLine = NO;
            cell.isBottomLine = YES;
            cell.isLeftLine = YES;
            cell.isRightLine = YES;
        }else if (indexPath.item == 4) {
            cell.isTopLine = NO;
            cell.isLeftLine = NO;
            cell.isBottomLine = YES;
            cell.isRightLine = YES;
        }else if (indexPath.item == 5) {
            cell.isTopLine = NO;
            cell.isLeftLine = NO;
            cell.isBottomLine = YES;
            cell.isRightLine = YES;
        }else if (indexPath.item == 6) {
            cell.isTopLine = NO;
            cell.isBottomLine = YES;
            cell.isLeftLine = YES;
            cell.isRightLine = YES;
        }else if (indexPath.item == 7) {
            cell.isTopLine = NO;
            cell.isLeftLine = NO;
            cell.isBottomLine = YES;
            cell.isRightLine = YES;
        }else if (indexPath.item == 8) {
            cell.isTopLine = NO;
            cell.isLeftLine = NO;
            cell.isBottomLine = YES;
            cell.isRightLine = YES;
        }else if (indexPath.item == 1 || indexPath.item == 2) {
            cell.isLeftLine = NO;
            cell.isRightLine = YES;
            cell.isTopLine = YES;
            cell.isBottomLine = YES;
        }else {
            cell.isLeftLine = YES;
            cell.isRightLine = YES;
            cell.isTopLine = YES;
            cell.isBottomLine = YES;
        }
    }else {
        ZCAbnormalInfoModel *infoM = (ZCAbnormalInfoModel *)self.cloakArray[indexPath.item];
        cell.num = infoM.countExcp;
        if (indexPath.item == 0 || indexPath.item == 2 || indexPath.item == 4 || indexPath.item == 6 || indexPath.item == 8) {
            cell.isTopLine = NO;
            cell.isLeftLine = NO;
            cell.isRightLine = NO;
            cell.isBottomLine = NO;
        }else {
            cell.isTopLine = YES;
            cell.isLeftLine = YES;
            cell.isBottomLine = YES;
            cell.isRightLine = YES;
        }
    }
    return cell;
}


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(abnormalSignView:SignButtonClickWithIndex:)]) {
        [self.delegate abnormalSignView:self SignButtonClickWithIndex:indexPath];
    }
}


#pragma mark - UICollectionViewFlowLayout
//每个cell的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.bounds.size.width / 3, self.bounds.size.width / 3);
}

//一行/列item内部的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

//上一行/列item和下一行/列item的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

//内边距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


@end
