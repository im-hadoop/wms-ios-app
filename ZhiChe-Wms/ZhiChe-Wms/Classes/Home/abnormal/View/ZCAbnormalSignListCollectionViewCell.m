//
//  ZCAbnormalSignListCollectionViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/22.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalSignListCollectionViewCell.h"

@interface ZCAbnormalSignListCollectionViewCell ()

@end

@implementation ZCAbnormalSignListCollectionViewCell
#pragma mark - 懒加载
- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imageView.image = [UIImage imageNamed:@"car4"];
        _imageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage:)];
        [_imageView addGestureRecognizer:tap];
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressImage:)];
        [_imageView addGestureRecognizer:longPress];
    }
    return _imageView;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.imageView];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setImage:(UIImage *)image {
    _image = image;
}

#pragma mark - 图片手势
- (void)tapImage:(UITapGestureRecognizer *)tap {
    if ([self.delegate respondsToSelector:@selector(abnormalSignListCollectionViewCellTapImageWithIndex:)]) {
        [self.delegate abnormalSignListCollectionViewCellTapImageWithIndex:self.indexPath];
    }
}

- (void)longPressImage:(UILongPressGestureRecognizer *)longPress {
    if (longPress.state == UIGestureRecognizerStateEnded) {
        if ([self.delegate respondsToSelector:@selector(abnormalSignListCollectionViewCellLongPressImageWithIndex:)]) {
            [self.delegate abnormalSignListCollectionViewCellLongPressImageWithIndex:self.indexPath];
        }
    }
    
}

@end
