//
//  ZCAbnormalDescTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZCAbnormalDescTableViewCellDelegate <NSObject>

- (void)abnormalDescTableViewCellCameraButtonClickWithIndex:(NSIndexPath *)index rectInScreen:(CGRect)rect;

@end

@interface ZCAbnormalDescTableViewCell : UITableViewCell
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, weak) id<ZCAbnormalDescTableViewCellDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *index;
@end
