//
//  ZCAbnormalSignListTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/22.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalSignListTableViewCell.h"
#import "ZCAbnormalSignListCollectionViewCell.h"
#import "ZCAbnormalModel.h"

static NSString *listCellID = @"ZCAbnormalSignListCollectionViewCell";

@interface ZCAbnormalSignListTableViewCell ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ZCAbnormalSignListCollectionViewCellDelegate>
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) UIView *lineView;
@end

@implementation ZCAbnormalSignListTableViewCell
#pragma mark - 懒加载
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"--------";
        _titleLabel.textColor = ZCColor(0x000000, 0.87);
        _titleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
    }
    return _titleLabel;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(space(328), space(20), space(324), space(100)) collectionViewLayout:flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[ZCAbnormalSignListCollectionViewCell class] forCellWithReuseIdentifier:listCellID];
    }
    return _collectionView;
}

- (UIButton *)deleteButton {
    if (!_deleteButton) {
        _deleteButton = [[UIButton alloc] init];
        [_deleteButton setImage:[UIImage imageNamed:@"delete_normal"] forState:UIControlStateNormal];
        [_deleteButton setImage:[UIImage imageNamed:@"delete_highlight"] forState:UIControlStateHighlighted];
        [_deleteButton addTarget:self action:@selector(deleteButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.collectionView];
        [self addSubview:self.deleteButton];
        [self addSubview:self.lineView];
        
    }
    return self;
}

#pragma mark - 按钮点击
- (void)deleteButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(abnormalSignListTableViewCellDeleteButtonClickWithIndex:)]) {
        [self.delegate abnormalSignListTableViewCellDeleteButtonClickWithIndex:self.indexPath];
    }
}

#pragma mark - 属性方法
- (void)setIsDelete:(BOOL)isDelete {
    _isDelete = isDelete;
    self.deleteButton.hidden = !isDelete;
}

- (void)setName:(NSString *)name {
    _name = name;
    self.titleLabel.text = name;
    NSMutableDictionary *attr = [NSMutableDictionary dictionary];
    [attr setObject:[UIFont systemFontOfSize:FontSize(26)] forKey:NSFontAttributeName];
    CGSize nameSize = [name boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, space(60)) options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    __weak typeof(self)weakSelf = self;
    [_titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(nameSize.width + space(10));
    }];

    [_collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleLabel.mas_right).offset(space(20));
        make.centerY.equalTo(weakSelf.titleLabel.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-space(110));
        make.height.mas_equalTo(space(100));
    }];

}

- (void)setImageArray:(NSMutableArray *)imageArray {
    _imageArray = imageArray;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZCAbnormalSignListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:listCellID forIndexPath:indexPath];
    ZCAbnormalListDetailModel *listDetailM = (ZCAbnormalListDetailModel *)self.imageArray[indexPath.item];
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:listDetailM.picUrl] placeholderImage:[UIImage imageNamed:@"placeholder_image"]];
    cell.indexPath = indexPath;
    cell.delegate = self;
    return cell;
}

#pragma mark - UICollectionViewFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(space(100), space(100));
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

//水平线上两个item的距离
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return space(12);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark - ZCAbnormalSignListCollectionViewCellDelegate
- (void)abnormalSignListCollectionViewCellTapImageWithIndex:(NSIndexPath *)index {
    if ([self.delegate respondsToSelector:@selector(abnormalSignListTableViewCellScanPhotoWithCellIndex:itemIndex:)]) {
        [self.delegate abnormalSignListTableViewCellScanPhotoWithCellIndex:self.indexPath itemIndex:index];
    }
}

- (void)abnormalSignListCollectionViewCellLongPressImageWithIndex:(NSIndexPath *)index {
//    [self.imageArray removeObject:self.imageArray[index.item]];
//    [self.collectionView reloadData];
//    if (self.imageArray.count == 0) {
//        [self deleteButtonClick:self.deleteButton];
//    }
    if ([self.delegate respondsToSelector:@selector(abnormalSignListTableViewCellLongPressImageWithCellIndex:itemIndex:)]) {
        [self.delegate abnormalSignListTableViewCellLongPressImageWithCellIndex:self.indexPath itemIndex:index];
    }
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
//        make.width.mas_equalTo(space(210));
    }];
    
    [_deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(space(2));
    }];
}


@end
