//
//  ZCAbnormalMissingTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalMissingTableViewCell.h"

@interface ZCAbnormalMissingTableViewCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *indicaterImageView;
@property (nonatomic, strong) UIView *lineView;
@end

@implementation ZCAbnormalMissingTableViewCell
#pragma mark - 懒加载
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"缺件";
        _titleLabel.textColor = ZCColor(0x000000, 0.54);
        _titleLabel.font = [UIFont systemFontOfSize:FontSize(30)];
    }
    return _titleLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.text = @"请选择";
        _contentLabel.textColor = ZCColor(0x000000, 0.54);
        _contentLabel.font = [UIFont systemFontOfSize:FontSize(30)];
        _contentLabel.textAlignment = NSTextAlignmentRight;
    }
    return _contentLabel;
}

- (UIImageView *)indicaterImageView {
    if (!_indicaterImageView) {
        _indicaterImageView = [[UIImageView alloc] init];
        _indicaterImageView.image = [UIImage imageNamed:@"箭头"];
    }
    return _indicaterImageView;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.contentLabel];
        [self addSubview:self.indicaterImageView];
        [self addSubview:self.lineView];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setContent:(NSString *)content {
    _content = content;
    self.contentLabel.text = content;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = title;
}

- (void)setContentColor:(UIColor *)contentColor {
    _contentColor = contentColor;
    self.contentLabel.textColor = contentColor;
}

- (void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
    self.titleLabel.textColor = titleColor;
}

- (void)setCanSelected:(BOOL)canSelected {
    _canSelected = canSelected;
    self.indicaterImageView.hidden = canSelected ? NO : YES;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.mas_equalTo(space(160));
    }];
    
    [_indicaterImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(7, 10));
    }];
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleLabel.mas_right).offset(space(20));
        make.right.equalTo(weakSelf.indicaterImageView.mas_left).offset(-space(10));
        make.centerY.equalTo(weakSelf.titleLabel.mas_centerY);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleLabel.mas_left);
        make.right.equalTo(weakSelf.indicaterImageView.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(space(2));
    }];
}


@end
