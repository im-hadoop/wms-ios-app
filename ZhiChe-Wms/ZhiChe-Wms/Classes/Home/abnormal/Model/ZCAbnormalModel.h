//
//  ZCAbnormalModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/21.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCAbnormalModel : NSObject

@property (nonatomic, copy) NSString *token;                //用户token
@property (nonatomic, copy) NSString *whCode;               //仓库code
@property (nonatomic, copy) NSString *userCode;             //用户编码
@property (nonatomic, copy) NSString *originCode;           //起运地编码
@property (nonatomic, copy) NSString *originName;           //起运地名称
@property (nonatomic, copy) NSString *destCode;             //目的地编码
@property (nonatomic, copy) NSString *destName;             //目的地名称
@property (nonatomic, copy) NSString *vin;                  //VIN码(车架号)
@property (nonatomic, copy) NSString *orderNo;              //订单号
@property (nonatomic, copy) NSString *wayBillNo;            //运单号
@property (nonatomic, copy) NSString *taskType;             //任务类型(10：寻车，20：移车，30：提车)
@property (nonatomic, copy) NSString *taskStatus;           //任务状态(10：创建，20：开始，30：完成，50：取消)
@property (nonatomic, copy) NSString *orderReleaseGid;      //系统单号
@property (nonatomic, copy) NSString *dispatchNo;           //指令号
@property (nonatomic, copy) NSString *taskId;               //任务Id
@property (nonatomic, copy) NSString *vehicle;              //车型
@property (nonatomic, copy) NSString *stanVehicleType;      //标准车型
@property (nonatomic, copy) NSString *otmStatus;
@property (nonatomic, copy) NSString *driverCode;           //司机编码
@property (nonatomic, copy) NSString *driverName;           //司机名称
@property (nonatomic, copy) NSString *driverPhone;          //司机电话
@property (nonatomic, copy) NSString *startTime;            //开始时间
@property (nonatomic, copy) NSString *finishTime;           //完成时间
@property (nonatomic, copy) NSString *releaseId;            //系统单号
@property (nonatomic, copy) NSString *qrCode;               //二维码
@property (nonatomic, copy) NSString *taskCreate;           //任务生成时间
@property (nonatomic, copy) NSString *taskStart;            //任务开始时间
@property (nonatomic, copy) NSString *taskFinish;           //任务完成时间
@property (nonatomic, copy) NSString *taskNode;             //事件节点
@property (nonatomic, copy) NSString *bindTime;             //二维码绑定时间
@property (nonatomic, copy) NSString *bindId;
@property (nonatomic, copy) NSString *isCanSend;            //是否发运

@end

//九宫格异常信息
@interface ZCAbnormalInfoModel : NSObject

@property (nonatomic, copy) NSString *level1Code;      //异常区域编码
@property (nonatomic, copy) NSString *level1Name;      //异常区域名称
@property (nonatomic, copy) NSString *level2Code;      //异常位置编码
@property (nonatomic, copy) NSString *level2Name;      //异常位置名称
@property (nonatomic, copy) NSString *level3Code;      //异常类别编码
@property (nonatomic, copy) NSString *level3Sort;      //异常类别排序
@property (nonatomic, copy) NSString *level3Name;      //异常类别名称
@property (nonatomic, copy) NSString *countExcp;       //异常数
@property (nonatomic, copy) NSString *vin;             //车架号
@property (nonatomic, copy) NSString *sort;            //异常排序
@property (nonatomic, copy) NSString *excpTime;        //异常标记时间
@property (nonatomic, copy) NSString *picId;           //图片Id
@property (nonatomic, copy) NSString *exceptionId;     //异常Id
@property (nonatomic, copy) NSString *storageServer;   //存储服务器(qiniu:七牛)
@property (nonatomic, copy) NSString *size;            //文件大小
@property (nonatomic, copy) NSString *picStatus;       //文件状态(10：正常，20：删除)
@property (nonatomic, copy) NSString *picKey;          //图片key
@property (nonatomic, copy) NSString *picUrl;          //图片url
@property (nonatomic, copy) NSString *picGmtCreate;    //创建时间
@property (nonatomic, copy) NSString *picGmtModified;  //修改时间


@end

//标记部位

@interface ZCAbnormalListDetailModel : NSObject

@property (nonatomic, copy) NSString *level1Code;      //异常区域编码
@property (nonatomic, copy) NSString *level1Name;      //异常区域名称
@property (nonatomic, copy) NSString *level2Code;      //异常位置编码
@property (nonatomic, copy) NSString *level2Name;      //异常位置名称
@property (nonatomic, copy) NSString *level3Code;      //异常类别编码
@property (nonatomic, copy) NSString *level3Sort;      //异常类别排序
@property (nonatomic, copy) NSString *level3Name;      //异常类别名称
@property (nonatomic, copy) NSString *countExcp;       //异常数
@property (nonatomic, copy) NSString *vin;             //车架号
@property (nonatomic, copy) NSString *sort;            //排序
@property (nonatomic, copy) NSString *excpTime;        //异常标记时间
@property (nonatomic, copy) NSString *picId;           //图片Id
@property (nonatomic, copy) NSString *exceptionId;     //异常Id
@property (nonatomic, copy) NSString *storageServer;   //存储服务器(qiniu:七牛)
@property (nonatomic, copy) NSString *size;            //文件大小
@property (nonatomic, copy) NSString *picStatus;       //状态(10：正常，20：删除)
@property (nonatomic, copy) NSString *picKey;          //图片key
@property (nonatomic, copy) NSString *picUrl;          //图片url
@property (nonatomic, copy) NSString *picGmtCreate;    //创建时间
@property (nonatomic, copy) NSString *picGmtModified;  //修改时间

@end

@interface ZCAbnormalListModel : NSObject

@property (nonatomic, copy) NSString *listId;
@property (nonatomic, copy) NSString *level1Code;    //异常区域编码
@property (nonatomic, copy) NSString *level1Name;    //异常区域名称
@property (nonatomic, copy) NSString *level1Sort;    //异常区域排序
@property (nonatomic, copy) NSString *level2Code;    //异常位置编码
@property (nonatomic, copy) NSString *level2Name;    //异常位置名称
@property (nonatomic, copy) NSString *level2Sort;    //异常位置排序
@property (nonatomic, copy) NSString *vin;           //车架号
@property (nonatomic, copy) NSString *orderNo;       //订单号
@property (nonatomic, strong) NSArray<ZCAbnormalListDetailModel *> *exceptionDetail;
@property (nonatomic, assign) CGFloat rowHeight;

@end

//标记异常列表

@interface ZCAbnormalSignModel : NSObject

@property (nonatomic, copy) NSString *vin;             //车架号
@property (nonatomic, copy) NSString *vehicle;         //车型
@property (nonatomic, copy) NSString *orderNo;         //订单号
@property (nonatomic, copy) NSString *missName;        //缺件
@property (nonatomic, copy) NSString *taskId;          //任务Id
@property (nonatomic, copy) NSString *taskType;        //任务类型
@property (nonatomic, copy) NSString *userCode;        //用户编码
@property (nonatomic, copy) NSString *exceptionId;     //异常Id
@property (nonatomic, copy) NSString *signId;
@property (nonatomic, copy) NSString *omsOrderNo;      //客户订单号
@property (nonatomic, copy) NSString *qrCode;          //二维码
@property (nonatomic, copy) NSString *taskNode;        //作业节点(0：指令，10：寻车，20：移车，30：提车，40：收车质检，41：入库移车，42：分配入库，50：出库备车，51：出库确认，60：装车交验)
@property (nonatomic, copy) NSString *businessDoc;     //业务单据
@property (nonatomic, copy) NSString *exLevel1;        //一级异常编码(异常区域编码)
@property (nonatomic, copy) NSString *level1Name;      //一级异常名称(异常区域名称)
@property (nonatomic, copy) NSString *exLevel2;        //二级异常编码(异常位置编码)
@property (nonatomic, copy) NSString *level2Name;      //二级异常名称(异常位置编码)
@property (nonatomic, copy) NSString *exLevel3;        //三级异常编码(异常类别编码)
@property (nonatomic, copy) NSString *level3Name;      //三级异常名称(异常类别名称)
@property (nonatomic, copy) NSString *exDescribe;      //描述
@property (nonatomic, copy) NSString *registerUser;    //登记人
@property (nonatomic, copy) NSString *registerTime;    //登记时间
@property (nonatomic, copy) NSString *involvedParty;   //责任方
@property (nonatomic, copy) NSString *dealStatus;      //处理状态(10：未处理，20：处理中，30：关闭，40：让步)
@property (nonatomic, copy) NSString *dealType;        //处理方式(10：返厂维修，20：出库维修，30：库内维修，40：带伤发运)
@property (nonatomic, copy) NSString *dealResultDesc;  //处理结果描述
@property (nonatomic, copy) NSString *dealEndTime;     //处理完成时间
@property (nonatomic, copy) NSString *status;          //状态(10：登记，20：处理，30：关闭)
@property (nonatomic, copy) NSString *userCreate;      //创建人
@property (nonatomic, copy) NSString *userModified;    //修改人
@property (nonatomic, copy) NSString *gmtCreate;       //创建时间
@property (nonatomic, copy) NSString *gmtModified;     //修改时间
@property (nonatomic, copy) NSString *picKey;          //图片key
@property (nonatomic, copy) NSString *picUrl;          //图片url
@property (nonatomic, copy) NSString *picUrlInfoList;  //图片url
@property (nonatomic, strong) NSArray<ZCAbnormalListDetailModel *> *exceptionDetails;

@end

//标记异常参数

@interface ZCAbnormalExceptionModel : NSObject

@property (nonatomic, copy) NSString *level2Code;
@property (nonatomic, copy) NSString *level2Name;
@property (nonatomic, copy) NSString *level3Code;
@property (nonatomic, copy) NSString *level3Name;
@property (nonatomic, copy) NSString *picKeys;

@end

//全部异常

@interface ZCAbnormalAllModel : NSObject

@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *pages;
@property (nonatomic, copy) NSString *current;
@property (nonatomic, copy) NSString *searchCount;
@property (nonatomic, copy) NSString *openSort;
@property (nonatomic, copy) NSString *orderByField;
@property (nonatomic, strong) NSArray<ZCAbnormalSignModel *> *records;
@property (nonatomic, copy) NSString *condition;
@property (nonatomic, copy) NSString *offsetCurrent;
@property (nonatomic, copy) NSString *asc;

@end

//缺件接口

@interface ZCAbnormalMisssingPartModel : NSObject

@property (nonatomic, copy) NSString *partId;         //id
@property (nonatomic, copy) NSString *code;           //工具编码
@property (nonatomic, copy) NSString *name;           //工具名称
@property (nonatomic, copy) NSString *describe;       //工具描述
@property (nonatomic, copy) NSString *parentCode;     //上级分类code
@property (nonatomic, copy) NSString *level;          //级别(工具名称)
@property (nonatomic, copy) NSString *sort;           //排序
@property (nonatomic, copy) NSString *status;         //状态(10：正常，20：禁用)
@property (nonatomic, copy) NSString *gmtCreate;      //创建时间
@property (nonatomic, copy) NSString *gmtModified;    //修改时间
@property (nonatomic, copy) NSString *isReigister;    //是否登记
@property (nonatomic, assign) BOOL selected;

@end

@interface ZCAbnormalMissingModel : NSObject

@property (nonatomic, copy) NSString *classId;            //id
@property (nonatomic, copy) NSString *level1Code;         //工具分类编码
@property (nonatomic, copy) NSString *level1Name;         //工具分类名称
@property (nonatomic, copy) NSString *parentCode;         //上级分类code
@property (nonatomic, copy) NSString *level;              //级别(工具分类)
@property (nonatomic, copy) NSString *sort;               //排序
@property (nonatomic, copy) NSString *status;             //状态(10：正常，20：禁用)
@property (nonatomic, copy) NSString *gmtCreate;          //创建时间
@property (nonatomic, copy) NSString *gmtModified;        //修改时间
@property (nonatomic, strong) NSArray<ZCAbnormalMisssingPartModel *> *childComponent;
@property (nonatomic, assign) CGFloat rowHeight;

@end
