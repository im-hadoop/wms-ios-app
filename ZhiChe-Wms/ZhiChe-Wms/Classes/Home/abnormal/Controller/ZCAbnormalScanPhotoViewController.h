//
//  ZCAbnormalScanPhotoViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/23.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCAbnormalScanPhotoViewController : UIViewController
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) NSIndexPath *nowIndex;
@end
