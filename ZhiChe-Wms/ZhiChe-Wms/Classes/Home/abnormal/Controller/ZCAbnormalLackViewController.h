//
//  ZCAbnormalLackViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/24.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^LackViewBlock)(NSString *missimgPart);

@interface ZCAbnormalLackViewController : UIViewController

@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *orderNo;
@property (nonatomic, copy) NSString *taskId;
@property (nonatomic, copy) LackViewBlock lackBlock;
@property (nonatomic, copy) NSString *taskType;
@end
