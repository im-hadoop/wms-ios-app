//
//  ZCVinQRCodeViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/23.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCVinQRCodeViewController : UIViewController

@property (nonatomic, copy) NSString *vin;

@end
