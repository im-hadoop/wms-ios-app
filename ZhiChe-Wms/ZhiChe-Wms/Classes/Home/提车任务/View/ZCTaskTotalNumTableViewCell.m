//
//  ZCTaskTotalNumTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/15.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCTaskTotalNumTableViewCell.h"

@interface ZCTaskTotalNumTableViewCell ()

@property (nonatomic, strong) UILabel *taskTitleLabel;
@property (nonatomic, strong) UILabel *taskNumLabel;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation ZCTaskTotalNumTableViewCell
#pragma mark - 懒加载
- (UILabel *)taskTitleLabel {
    if (!_taskTitleLabel) {
        _taskTitleLabel = [[UILabel alloc] init];
        _taskTitleLabel.text = @"任务总数";
        _taskTitleLabel.textColor = ZCColor(0x000000, 0.87);
        _taskTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _taskTitleLabel;
}

- (UILabel *)taskNumLabel {
    if (!_taskNumLabel) {
        _taskNumLabel = [[UILabel alloc] init];
        _taskNumLabel.text = @"------";
        _taskNumLabel.textColor = ZCColor(0x000000, 0.87);
        _taskNumLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _taskNumLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
        
    }
    return _lineView;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.taskTitleLabel];
        [self addSubview:self.taskNumLabel];
        [self addSubview:self.lineView];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setNum:(NSInteger)num {
    _num = num;
    _taskNumLabel.text = [NSString stringWithFormat:@"%zd",num];
}

- (void)setLine:(BOOL)line {
    _line = line;
    _lineView.hidden = !line;
}

- (void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
    _taskTitleLabel.textColor = titleColor;
}

- (void)setContentColor:(UIColor *)contentColor {
    _contentColor = contentColor;
    _taskNumLabel.textColor = contentColor;
}

- (void)setTitleFont:(UIFont *)titleFont {
    _titleFont = titleFont;
    _taskTitleLabel.font = titleFont;
}

- (void)setContentFont:(UIFont *)contentFont {
    _contentFont = contentFont;
    _taskNumLabel.font = contentFont;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    _taskTitleLabel.text = title;
}

- (void)setContent:(NSString *)content {
    _content = content;
    _taskNumLabel.text = content;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_taskTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_taskNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.taskTitleLabel.mas_centerY);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(space(2));
    }];
}

@end
