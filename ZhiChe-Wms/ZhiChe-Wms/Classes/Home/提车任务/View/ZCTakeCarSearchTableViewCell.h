//
//  ZCTakeCarSearchTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCTaskModel.h"

@interface ZCTakeCarSearchTableViewCell : UITableViewCell

@property (nonatomic, strong) ZCTaskModel *taskModel;

@end
