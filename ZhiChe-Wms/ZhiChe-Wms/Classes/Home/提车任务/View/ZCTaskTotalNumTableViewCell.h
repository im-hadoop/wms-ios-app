//
//  ZCTaskTotalNumTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/15.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCTaskTotalNumTableViewCell : UITableViewCell

@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) BOOL line;
@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, strong) UIColor *contentColor;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIFont *contentFont;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;

@end
