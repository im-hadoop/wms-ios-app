//
//  ZCStorageAllocationViewController.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/20.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCStorageAllocationViewController.h"
#import "UWRQViewController.h"
#import "ZCStorageAllocModel.h"
#import "ZCPutInStorageTableViewCell.h"
#import "ZCPutInStoragePlanDetailViewController.h"

static NSString *storageCellID = @"ZCPutInStorageTableViewCell";

@interface ZCStorageAllocationViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchControllerDelegate, UISearchResultsUpdating, uwRQDelegate, UISearchBarDelegate>
@property (nonatomic, strong) UITableViewController *tableViewVC;
@property (nonatomic, strong) UIView *searchView;
@property (nonatomic, strong) UISearchController *searchVC;
@property (nonatomic, strong) UIButton *qrButton;
@property (nonatomic, strong) NSMutableArray *listArray;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation ZCStorageAllocationViewController
#pragma mark - 懒加载
- (UIView *)searchView {
    if (!_searchView) {
        _searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREENWIDTH, 40)];
        _searchView.backgroundColor = [UIColor whiteColor];
    }
    return _searchView;
}

- (UITableViewController *)tableViewVC {
    if (!_tableViewVC) {
        _tableViewVC = [[UITableViewController alloc] initWithStyle:UITableViewStyleGrouped];
        //        _tableViewVC.tableView.frame =
        _tableViewVC.tableView.delegate = self;
        _tableViewVC.tableView.dataSource = self;
        _tableViewVC.tableView.showsVerticalScrollIndicator = NO;
        _tableViewVC.tableView.showsHorizontalScrollIndicator = NO;
        if (@available(iOS 11.0, *)) {
            _tableViewVC.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableViewVC.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
            _tableViewVC.tableView.scrollIndicatorInsets = _tableViewVC.tableView.contentInset;
            _tableViewVC.tableView.estimatedRowHeight = 0;
            _tableViewVC.tableView.estimatedSectionFooterHeight = 0;
            _tableViewVC.tableView.estimatedSectionHeaderHeight = 0;
        }
        [_tableViewVC.tableView registerClass:[ZCPutInStorageTableViewCell class] forCellReuseIdentifier:storageCellID];
        _tableViewVC.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        _tableViewVC.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    }
    return _tableViewVC;
}

-(UIButton *)qrButton {
    if (!_qrButton) {
        _qrButton = [[UIButton alloc] init];
        [_qrButton setImage:[UIImage imageNamed:@"scanQR"] forState:UIControlStateNormal];
        [_qrButton addTarget:self action:@selector(qrCodeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _qrButton;
}

- (UISearchController *)searchVC {
    if (!_searchVC) {
        _searchVC = [[UISearchController alloc] initWithSearchResultsController:self.tableViewVC];
        _searchVC.hidesNavigationBarDuringPresentation = YES;
        _searchVC.searchBar.placeholder = @"请输入订单号/车架号";
        _searchVC.delegate = self;
        _searchVC.searchResultsUpdater = self;
        [_searchVC.searchBar addSubview:self.qrButton];
        _searchVC.searchBar.delegate = self;
        __weak typeof(self)weakSelf = self;
        [_qrButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(weakSelf.searchVC.searchBar.mas_centerY);
            make.right.mas_equalTo(weakSelf.searchVC.searchBar.mas_right).offset(-15);
        }];
    }
    return _searchVC;
}

- (NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [NSMutableArray array];
    }
    return _listArray;
}

#pragma mark - 生命周期

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden = YES;
    self.navigationItem.title = @"分配入库";
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [leftButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.view.backgroundColor = [UIColor whiteColor];
    self.definesPresentationContext = YES;
    [self.view addSubview:self.searchView];
    [self.searchView addSubview:self.searchVC.searchBar];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.listArray removeAllObjects];
    if (self.searchVC.searchBar.text.length > 0) {
        [self loadData];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.hud hideAnimated:YES];
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.label.text = @"";
}

#pragma mark - 按钮点击
- (void)backToHome {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)qrCodeButtonClick:(UIButton *)sender {
    UWRQViewController *rqVC = [[UWRQViewController alloc] init];
    rqVC.delegate = self;
    [self.navigationController pushViewController:rqVC animated:YES];
}

#pragma mark - 网络请求
- (void)loadData {
    self.currentPage = 1;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSString *houseId = [[NSUserDefaults standardUserDefaults] objectForKey:UserStoreId];
    [param setObject:houseId forKey:@"houseId"];
    [param setObject:self.searchVC.searchBar.text forKey:@"key"];
    [param setObject:@(self.currentPage) forKey:@"current"];
    [param setObject:@(10) forKey:@"size"];
    
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.label.text = @"";
    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:PutInStorageAllocList params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                [self.listArray removeAllObjects];
                NSArray *dataArray = responseObject[@"data"];
                for (NSDictionary *dic in dataArray) {
                    ZCStorageAllocModel *allocModel = [ZCStorageAllocModel yy_modelWithDictionary:dic];
                    [weakSelf.listArray addObject:allocModel];
                }
                [weakSelf.tableViewVC.tableView reloadData];
                [weakSelf.tableViewVC.tableView.mj_header endRefreshing];
                if (dataArray.count < 10) {
                    [weakSelf.tableViewVC.tableView.mj_footer endRefreshingWithNoMoreData];
                }else {
                    [weakSelf.tableViewVC.tableView.mj_footer resetNoMoreData];
                }
            }else {
                [weakSelf.tableViewVC.tableView.mj_header endRefreshing];
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            [weakSelf.tableViewVC.tableView.mj_header endRefreshing];
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

- (void)loadMoreData {
    self.currentPage++;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSString *houseId = [[NSUserDefaults standardUserDefaults] objectForKey:UserStoreId];
    [param setObject:houseId forKey:@"houseId"];
    [param setObject:self.searchVC.searchBar.text forKey:@"key"];
    [param setObject:@(self.currentPage) forKey:@"current"];
    [param setObject:@(10) forKey:@"size"];
    
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.label.text = @"";
    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:PutInStorageAllocList params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                NSArray *dataArray = responseObject[@"data"];
                for (NSDictionary *dic in dataArray) {
                    ZCStorageAllocModel *allocModel = [ZCStorageAllocModel yy_modelWithDictionary:dic];
                    [weakSelf.listArray addObject:allocModel];
                }
                [weakSelf.tableViewVC.tableView reloadData];
                if (dataArray.count < 10) {
                    [weakSelf.tableViewVC.tableView.mj_footer endRefreshingWithNoMoreData];
                    weakSelf.currentPage--;
                }else {
                    [weakSelf.tableViewVC.tableView.mj_footer resetNoMoreData];
                }
            }else {
                weakSelf.currentPage--;
                [weakSelf.tableViewVC.tableView.mj_footer endRefreshing];
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.currentPage--;
            [weakSelf.tableViewVC.tableView.mj_footer endRefreshing];
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

#pragma mark - uwRQDelegate
- (void)uwRQFinshedScan:(NSString *)result {
    NSArray *resArr = [result componentsSeparatedByString:@","];
    NSString *keyId = [resArr firstObject];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSString *houseId = [[NSUserDefaults standardUserDefaults] objectForKey:UserStoreId];
    [param setObject:houseId forKey:@"houseId"];
    [param setObject:keyId forKey:@"key"];
    [param setObject:@"SCAN" forKey:@"visitType"];
    
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.label.text = @"";
    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:PutInStoragePlanDetail params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                if (![responseObject[@"data"] isEqual:[NSNull null]]) {
                    [weakSelf.hud hideAnimated:YES];
                    ZCPutInStoragePlanDetailViewController *detailVC = [[ZCPutInStoragePlanDetailViewController alloc] init];
                    detailVC.houseId = houseId;
                    detailVC.key = keyId;
                    detailVC.isClick = NO;
                    detailVC.fromPlan = NO;
                    
                    [weakSelf.navigationController pushViewController:detailVC animated:YES];
                }else {
                    weakSelf.hud.mode = MBProgressHUDModeText;
                    weakSelf.hud.label.text = @"未查到对应信息";
                    [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
                }
                
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
    
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.tableViewVC.view addSubview:self.hud];
    [self loadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.listArray removeAllObjects];
    [self.tableViewVC.tableView reloadData];
}

#pragma mark - UISearchControllerDelegate
- (void)willPresentSearchController:(UISearchController *)searchController {
    self.qrButton.hidden = YES;
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    self.qrButton.hidden = NO;
}

#pragma mark - UISearchResultsUpdating
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    if (searchController.isActive) {
        
        searchController.searchBar.showsCancelButton = YES;
        UIButton *cancelButton = [searchController.searchBar valueForKey:@"cancelButton"];
        if (cancelButton) {
            [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
            [cancelButton setTitleColor:ZCColor(0x000000, 0.87) forState:UIControlStateNormal];
        }
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.listArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPutInStorageTableViewCell *storageCell = [tableView dequeueReusableCellWithIdentifier:storageCellID forIndexPath:indexPath];
    storageCell.selectionStyle = UITableViewCellSelectionStyleNone;
    storageCell.typeTitle = @"收货仓库";
    storageCell.statusTitle = @"状    态";
    ZCStorageAllocModel *allocationM = (ZCStorageAllocModel *)self.listArray[indexPath.section];
    storageCell.allocationModel = allocationM;

    return storageCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return space(360);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return space(20);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPutInStoragePlanDetailViewController *detailVC = [[ZCPutInStoragePlanDetailViewController alloc] init];
    ZCStorageAllocModel *allocModel = (ZCStorageAllocModel *)self.listArray[indexPath.section];
    detailVC.houseId = allocModel.storeHouseId;
    detailVC.key = allocModel.Id;
    detailVC.isClick = YES;
    detailVC.fromPlan = NO;
    
    [self.navigationController pushViewController:detailVC animated:YES];
}


@end
