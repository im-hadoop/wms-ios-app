//
//  ZCPutInStorageTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/29.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCPutInStoragePlanModel.h"
#import "ZCQualityTestingModel.h"
#import "ZCStorageAllocModel.h"
#import "ZCOutboundPlanModel.h"
#import "ZCPreparationModel.h"
#import "ZCAdjustmentModel.h"
#import "ZCLoadingModel.h"
#import "ZCCancellingStockModel.h"

@interface ZCPutInStorageTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString *typeTitle;
@property (nonatomic, copy) NSString *statusTitle;
@property (nonatomic, copy) NSString *endTitle;

//入库计划模型
@property (nonatomic, strong) ZCStoragePlanInfoModel *planModel;

//收车质检模型
@property (nonatomic, strong) ZCQualityTestingModel *testingModel;

//分配入库模型
@property (nonatomic, strong) ZCStorageAllocModel *allocationModel;

//出库计划模型
@property (nonatomic, strong) ZCOutboundPlanInfoModel *obPlanModel;

//出库确认模型
@property (nonatomic, strong) ZCOutboundPlanDetailModel *obCommitModel;

//备料任务模型
@property (nonatomic, strong) ZCPreparationModel *preparationModel;

@property (nonatomic, assign) BOOL fromLocation;

//库位调整
@property (nonatomic, strong) ZCAdjustmentInfoModel *adjustmentInfoModel;

//装车交验
@property (nonatomic, strong) ZCLoadingInfoModel *loadingInfoModel;

//退库任务
@property (nonatomic, strong) ZCCancellingStockDetailModel *cancellingModel;

@end
