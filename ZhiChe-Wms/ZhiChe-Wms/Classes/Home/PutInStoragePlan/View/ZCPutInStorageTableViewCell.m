//
//  ZCPutInStorageTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/29.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCPutInStorageTableViewCell.h"


@interface ZCPutInStorageTableViewCell ()
@property (nonatomic, strong) UILabel *orderTitleLabel;
@property (nonatomic, strong) UILabel *orderNumLabel;
@property (nonatomic, strong) UILabel *startTitleLabel;
@property (nonatomic, strong) UILabel *startLabel;
@property (nonatomic, strong) UILabel *typeTitleLabel;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *endTitleLabel;
@property (nonatomic, strong) UILabel *endLabel;
@property (nonatomic, strong) UILabel *statusTitleLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UILabel *carTitleLabel;
@property (nonatomic, strong) UILabel *carNumLabel;

@end


@implementation ZCPutInStorageTableViewCell
- (UILabel *)orderTitleLabel {
    if (!_orderTitleLabel) {
        _orderTitleLabel = [[UILabel alloc] init];
        _orderTitleLabel.text = @"订单号";
        _orderTitleLabel.textColor = ZCColor(0x000000, 0.87);
        _orderTitleLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _orderTitleLabel;
}

- (UILabel *)orderNumLabel {
    if (!_orderNumLabel) {
        _orderNumLabel = [[UILabel alloc] init];
        _orderNumLabel.text = @"20288837";
        _orderNumLabel.textColor = ZCColor(0x000000, 0.87);
        _orderNumLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _orderNumLabel;
}

- (UILabel *)startTitleLabel {
    if (!_startTitleLabel) {
        _startTitleLabel = [[UILabel alloc] init];
        _startTitleLabel.text = @"客户";
        _startTitleLabel.textColor = ZCColor(0x000000, 0.34);
        _startTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _startTitleLabel;
}

- (UILabel *)startLabel {
    if (!_startLabel) {
        _startLabel = [[UILabel alloc] init];
        _startLabel.text = @"全顺库";
        _startLabel.textColor = ZCColor(0x000000, 0.54);
        _startLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _startLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _startLabel.numberOfLines = 0;
    }
    return _startLabel;
}

- (UILabel *)typeTitleLabel {
    if (!_typeTitleLabel) {
        _typeTitleLabel = [[UILabel alloc] init];
        _typeTitleLabel.text = @"车型";
        _typeTitleLabel.textColor = ZCColor(0x000000, 0.34);
        _typeTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _typeTitleLabel.textAlignment = NSTextAlignmentRight;
    }
    return _typeTitleLabel;
}

- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        _typeLabel.text = @"提车任务";
        _typeLabel.textColor = ZCColor(0x000000, 0.34);
        _typeLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _typeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _typeLabel.numberOfLines = 0;
    }
    return _typeLabel;
}

- (UILabel *)endTitleLabel {
    if (!_endTitleLabel) {
        _endTitleLabel = [[UILabel alloc] init];
        _endTitleLabel.text = @"日期";
        _endTitleLabel.textColor = ZCColor(0x000000, 0.34);
        _endTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _endTitleLabel;
}

- (UILabel *)endLabel {
    if (!_endLabel) {
        _endLabel = [[UILabel alloc] init];
        _endLabel.text = @"电网库";
        _endLabel.textColor = ZCColor(0x000000, 0.54);
        _endLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _endLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    return _endLabel;
}

- (UILabel *)statusTitleLabel {
    if (!_statusTitleLabel) {
        _statusTitleLabel = [[UILabel alloc] init];
        _statusTitleLabel.text = @"状态";
        _statusTitleLabel.textColor = ZCColor(0x000000, 0.34);
        _statusTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _statusTitleLabel.textAlignment = NSTextAlignmentRight;
    }
    return _statusTitleLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.text = @"创建";
        _statusLabel.textColor = ZCColor(0xff8213, 1);
        _statusLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _statusLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

- (UILabel *)carTitleLabel {
    if (!_carTitleLabel) {
        _carTitleLabel = [[UILabel alloc] init];
        _carTitleLabel.text = @"车架号";
        _carTitleLabel.textColor = ZCColor(0x000000, 0.87);
        _carTitleLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _carTitleLabel;
}

- (UILabel *)carNumLabel {
    if (!_carNumLabel) {
        _carNumLabel = [[UILabel alloc] init];
        _carNumLabel.text = @"-----";
        _carNumLabel.textColor = ZCColor(0x000000, 0.87);
        _carNumLabel.font = [UIFont boldSystemFontOfSize:FontSize(28)];
    }
    return _carNumLabel;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.orderTitleLabel];
        [self addSubview:self.orderNumLabel];
        [self addSubview:self.startTitleLabel];
        [self addSubview:self.startLabel];
        [self addSubview:self.typeTitleLabel];
        [self addSubview:self.typeLabel];
        [self addSubview:self.endTitleLabel];
        [self addSubview:self.endLabel];
        [self addSubview:self.statusTitleLabel];
        [self addSubview:self.statusLabel];
        [self addSubview:self.lineView];
        [self addSubview:self.carTitleLabel];
        [self addSubview:self.carNumLabel];
        
    }
    return self;
}

- (void)setPlanModel:(ZCStoragePlanInfoModel *)planModel {
    _planModel = planModel;
    _orderNumLabel.text = planModel.ownerOrderNo;
    _startLabel.text = planModel.ownerId;
    NSString *dateStr = [planModel.expectDate substringToIndex:10];
    _endLabel.text = dateStr;
    _typeLabel.text = planModel.materielId;
    _statusLabel.text = [planModel.status integerValue] == 10 ? @"未入库" : @"已入库";
    _carNumLabel.text = planModel.lotNo1;
    
}

- (void)setTestingModel:(ZCQualityTestingModel *)testingModel {
    _testingModel = testingModel;
    _orderNumLabel.text = testingModel.ownerOrderNo;
    _startLabel.text = testingModel.ownerId;
    NSString *dateStr = [testingModel.recvDate substringToIndex:10];
    _endLabel.text = dateStr;
    _typeLabel.text = testingModel.storeHouseName;
    _statusLabel.text = [testingModel.status integerValue] == 0 ? @"未质检" : @"已质检";
    _carNumLabel.text = testingModel.lotNo1;
}

- (void)setAllocationModel:(ZCStorageAllocModel *)allocationModel {
    _allocationModel = allocationModel;
    _orderNumLabel.text = allocationModel.ownerOrderNo;
    _startLabel.text = allocationModel.ownerName;
    NSString *dateStr = [allocationModel.recvDate substringToIndex:10];
    _endLabel.text = dateStr;
    _typeLabel.text = allocationModel.storeHouseName;
    _statusLabel.text = [allocationModel.status integerValue] == 10 ? @"未入库" : @"已入库";
    _carNumLabel.text = allocationModel.lotNo1;
}

- (void)setObPlanModel:(ZCOutboundPlanInfoModel *)obPlanModel {
    _obPlanModel = obPlanModel;
    _orderNumLabel.text = obPlanModel.ownerOrderNo;
    _startLabel.text = obPlanModel.ownerId;
    NSString *dateStr = [obPlanModel.expectDate substringToIndex:10];
    _endLabel.text = dateStr;
    _typeLabel.text = obPlanModel.materielId;
    switch ([obPlanModel.noticeLineStatus integerValue]) {
        case 10:
            _statusLabel.text = @"未出库";
            break;
        case 20:
            _statusLabel.text = @"部分出库";
            break;
        case 30:
            _statusLabel.text = @"已出库";
            break;
        case 40:
            _statusLabel.text = @"关闭出库";
            break;
        case 50:
            _statusLabel.text = @"取消";
            break;
        default:
            _statusLabel.text = @"状态未知";
            break;
    }
    _carNumLabel.text = obPlanModel.lotNo1;
}

- (void)setObCommitModel:(ZCOutboundPlanDetailModel *)obCommitModel {
    _obCommitModel = obCommitModel;
    _orderNumLabel.text = obCommitModel.ownerOrderNo;
    _startLabel.text = obCommitModel.ownerName;
    _typeLabel.text = obCommitModel.storeHouseName;
    NSString *dateStr = [obCommitModel.recvDate substringToIndex:10];
    _endLabel.text = dateStr;
    switch ([obCommitModel.status integerValue]) {
        case 10:
            _statusLabel.text = @"未出库";
            break;
        case 20:
            _statusLabel.text = @"部分出库";
            break;
        case 30:
            _statusLabel.text = @"已出库";
            break;
        case 40:
            _statusLabel.text = @"关闭出库";
            break;
        case 50:
            _statusLabel.text = @"取消";
            break;
        default:
            _statusLabel.text = @"状态未知";
            break;
    }
    _carNumLabel.text = obCommitModel.lotNo1;
}

- (void)setPreparationModel:(ZCPreparationModel *)preparationModel {
    _preparationModel = preparationModel;
    _orderNumLabel.text = preparationModel.ownerOrderNo;
    _startLabel.text = preparationModel.ownerId;  //车辆列表接口用ownerName
    _typeLabel.text = preparationModel.storeHouseName; //车辆列表接口用storeHouseName
    NSString *dateStr = [preparationModel.planTime substringToIndex:10];//车辆列表接口用recvDate
    _endLabel.text = dateStr;
    NSString *currentStatus = @"";
    if ([preparationModel.status integerValue] == 10) {
        currentStatus = @"未领取";
    }else {
        if ([preparationModel.vehicleCheckoutStatus integerValue] == 0) {
            currentStatus = @"未校验";
        }else if ([preparationModel.vehicleCheckoutStatus integerValue] == 1) {
            currentStatus = @"已校验";
        }
    }
    _statusLabel.text = currentStatus;
    _carNumLabel.text = preparationModel.lotNo1;
}

- (void)setAdjustmentInfoModel:(ZCAdjustmentInfoModel *)adjustmentInfoModel {
    _adjustmentInfoModel = adjustmentInfoModel;
    _startLabel.text = adjustmentInfoModel.ownerId;
    _typeLabel.text = adjustmentInfoModel.storeHouseName;
    _endLabel.text = adjustmentInfoModel.materielId;
    _statusLabel.text = adjustmentInfoModel.locationName;
    _carNumLabel.text = adjustmentInfoModel.lotNo1;
}

- (void)setLoadingInfoModel:(ZCLoadingInfoModel *)loadingInfoModel {
    _loadingInfoModel = loadingInfoModel;
    _orderNumLabel.text = loadingInfoModel.cusOrderNo;
    _startLabel.text = loadingInfoModel.customerId;
    _typeLabel.text = loadingInfoModel.cusVehicleType;
    NSString *dataStr = [loadingInfoModel.gmtCreate substringToIndex:10];
    _endLabel.text = dataStr;
    _statusLabel.text = [loadingInfoModel.status isEqualToString:@"BS_CREATED"] ? @"已调度" : @"已交验";
    _carNumLabel.text = loadingInfoModel.vin;
}

- (void)setCancellingModel:(ZCCancellingStockDetailModel *)cancellingModel {
    _cancellingModel = cancellingModel;
    _orderNumLabel.text = cancellingModel.ownerOrderNo;
    _startLabel.text = cancellingModel.ownerId;
    _typeLabel.text = cancellingModel.stanVehicleType;
    NSString *dataStr = [cancellingModel.gmtCreate substringToIndex:10];
    _endLabel.text = dataStr;
    if ([cancellingModel.statusReceive integerValue] == 10) {
        _statusLabel.text = @"未领取";
    }else if ([cancellingModel.statusReceive integerValue] == 20) {
        _statusLabel.text = @"已领取";
    }else if ([cancellingModel.statusReceive integerValue] == 30) {
        _statusLabel.text = @"已完成";
    }
    _carNumLabel.text = cancellingModel.vin;
}

- (void)setTypeTitle:(NSString *)typeTitle {
    _typeTitle = typeTitle;
    self.typeTitleLabel.text = typeTitle;
    NSMutableDictionary *attr = [NSMutableDictionary dictionary];
    [attr setObject:[UIFont systemFontOfSize:FontSize(28)] forKey:NSFontAttributeName];
    CGSize size = [self.typeTitleLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    __weak typeof(self)weakSelf = self;
    [self.typeTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startLabel.mas_centerY);
        make.left.equalTo(weakSelf.mas_centerX).offset(space(20));
        make.width.mas_equalTo(size.width + space(10));
    }];
    
    [self.statusTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endLabel.mas_centerY);
        make.left.equalTo(weakSelf.typeTitleLabel.mas_left);
        make.width.equalTo(weakSelf.typeTitleLabel.mas_width);
    }];
    
}

- (void)setStatusTitle:(NSString *)statusTitle {
    _statusTitle = statusTitle;
    self.statusTitleLabel.text = statusTitle;
}

- (void)setEndTitle:(NSString *)endTitle {
    _endTitle = endTitle;
    self.endTitleLabel.text = endTitle;
}

- (void)setFromLocation:(BOOL)fromLocation {
    _fromLocation = fromLocation;
    self.orderTitleLabel.hidden = fromLocation;
    self.orderNumLabel.hidden = fromLocation;
}

+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_orderTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(space(40));
        make.left.equalTo(weakSelf.mas_left).offset(space(30));
        make.width.mas_equalTo(space(120));
    }];
    
    [_orderNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.orderTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.orderTitleLabel.mas_right).offset(space(20));
    }];
    
    [_startTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(30));
        make.size.mas_equalTo(CGSizeMake(space(80), space(40)));
        if (self.fromLocation) {
            make.top.equalTo(weakSelf.mas_top).offset(space(40));
        }else {
            make.top.equalTo(weakSelf.orderTitleLabel.mas_bottom).offset(space(30));
        }
    }];
    
    [_startLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.startTitleLabel.mas_right).offset(space(10));
        make.right.equalTo(weakSelf.mas_centerX).offset(space(10));
    }];
    
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startLabel.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.left.equalTo(weakSelf.typeTitleLabel.mas_right).offset(space(20));
    }];
    
    [_endTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.startTitleLabel.mas_bottom).offset(space(30));
        make.centerX.equalTo(weakSelf.startTitleLabel.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(space(80), space(40)));
    }];

    [_endLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.startLabel.mas_left);
        make.right.equalTo(weakSelf.mas_centerX).offset(space(10));
    }];

    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endLabel.mas_centerY);
        make.left.equalTo(weakSelf.typeLabel.mas_left);
        make.right.equalTo(weakSelf.typeLabel.mas_right);
    }];

    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.endTitleLabel.mas_bottom).offset(space(30));
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(2));
    }];

    [_carTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.lineView.mas_bottom).offset(space(40));
        make.left.equalTo(weakSelf.orderTitleLabel.mas_left);
    }];

    [_carNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.carTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.orderNumLabel.mas_left);
    }];
}



@end
