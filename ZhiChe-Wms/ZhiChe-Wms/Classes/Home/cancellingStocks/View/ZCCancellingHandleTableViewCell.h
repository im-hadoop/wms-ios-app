//
//  ZCCancellingHandleTableViewCell.h
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/21.
//  Copyright © 2018 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ZCCancellingHandleTableViewCellDelegate <NSObject>

- (void)cancellingHandleTableViewCellTakeTaskAction:(UIButton *)sender;

@end

@interface ZCCancellingHandleTableViewCell : UITableViewCell

@property (nonatomic, weak) id<ZCCancellingHandleTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
